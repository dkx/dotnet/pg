﻿using Microsoft.CodeAnalysis;

namespace DKX.Pg.AOT.Exceptions
{
	public sealed class EntityIsNotPartialException : DiagnosticsAwareException
	{
		private readonly INamedTypeSymbol _classType;

		private readonly Location _location;
		
		public EntityIsNotPartialException(INamedTypeSymbol classType, Location location) : base($"Entity class '{classType.ContainingNamespace}.{classType.Name}' must be marked as partial")
		{
			_classType = classType;
			_location = location;
		}

		public override Diagnostic ToDiagnostic()
		{
			return Diagnostic.Create(DiagnosticDescriptors.EntityNotPartial, _location, _classType.ContainingNamespace.ToString(), _classType.Name);
		}
	}
}
