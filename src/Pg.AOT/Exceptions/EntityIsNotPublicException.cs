﻿using Microsoft.CodeAnalysis;

namespace DKX.Pg.AOT.Exceptions
{
	public sealed class EntityIsNotPublicException : DiagnosticsAwareException
	{
		private readonly INamedTypeSymbol _classType;

		private readonly Location _location;
		
		public EntityIsNotPublicException(INamedTypeSymbol classType, Location location) : base($"Entity class '{classType.ContainingNamespace}.{classType.Name}' must be marked as public")
		{
			_classType = classType;
			_location = location;
		}

		public override Diagnostic ToDiagnostic()
		{
			return Diagnostic.Create(DiagnosticDescriptors.EntityNotPublic, _location, _classType.ContainingNamespace.ToString(), _classType.Name);
		}
	}
}
