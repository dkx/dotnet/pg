﻿using System;
using Microsoft.CodeAnalysis;

namespace DKX.Pg.AOT.Exceptions
{
	public abstract class DiagnosticsAwareException : Exception
	{
		protected DiagnosticsAwareException(string message) : base(message)
		{
		}
		
		public abstract Diagnostic ToDiagnostic();
	}
}
