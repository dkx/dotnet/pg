﻿using Microsoft.CodeAnalysis;

namespace DKX.Pg.AOT.Exceptions
{
	public sealed class CouldNotLoadClassInfoException : DiagnosticsAwareException
	{
		private readonly SyntaxToken _identifier;

		private readonly Location _location;

		public CouldNotLoadClassInfoException(SyntaxToken identifier, Location location) : base($"Could not load class info for '{identifier}'")
		{
			_identifier = identifier;
			_location = location;
		}

		public override Diagnostic ToDiagnostic()
		{
			return Diagnostic.Create(DiagnosticDescriptors.ErrorLoadingClassInfo, _location, _identifier.ToString());
		}
	}
}
