﻿using System.Collections.Generic;
using DKX.Pg.AOT.CodeBuilder;
using DKX.Pg.AOT.Metadata;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace DKX.Pg.AOT.Entities
{
	internal interface IEntitySyntaxType
	{
		ClassKind Kind { get; }
		
		BaseTypeDeclarationSyntax Declaration { get; }
		
		SyntaxTree SyntaxTree { get; }
		
		SyntaxToken Identifier { get; }
		
		SyntaxTokenList Modifiers { get; }

		SyntaxList<AttributeListSyntax> AttributeLists { get; }
		
		EntityMetadata ToEntityMetadata(SemanticModel model);

		IEnumerable<SyntaxNode> GetChildNodes();
	}
}
