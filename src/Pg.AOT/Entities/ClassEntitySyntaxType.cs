﻿using System.Collections.Generic;
using DKX.Pg.AOT.CodeBuilder;
using DKX.Pg.AOT.Metadata;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace DKX.Pg.AOT.Entities
{
	internal sealed class ClassEntitySyntaxType : IEntitySyntaxType
	{
		private readonly ClassDeclarationSyntax _declaration;
		
		public ClassEntitySyntaxType(ClassDeclarationSyntax declaration)
		{
			_declaration = declaration;
		}

		public ClassKind Kind => ClassKind.Class;

		public BaseTypeDeclarationSyntax Declaration => _declaration;

		public SyntaxTree SyntaxTree => _declaration.SyntaxTree;

		public SyntaxToken Identifier => _declaration.Identifier;

		public SyntaxTokenList Modifiers => _declaration.Modifiers;

		public SyntaxList<AttributeListSyntax> AttributeLists => _declaration.AttributeLists;

		public EntityMetadata ToEntityMetadata(SemanticModel model)
		{
			return EntityMetadataFactory.Create(model, this);
		}

		public IEnumerable<SyntaxNode> GetChildNodes()
		{
			return _declaration.ChildNodes();
		}
	}
}
