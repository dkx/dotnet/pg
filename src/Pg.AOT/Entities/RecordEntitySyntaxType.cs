﻿using System.Collections.Generic;
using DKX.Pg.AOT.CodeBuilder;
using DKX.Pg.AOT.Metadata;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace DKX.Pg.AOT.Entities
{
	internal sealed class RecordEntitySyntaxType : IEntitySyntaxType
	{
		private readonly RecordDeclarationSyntax _declaration;
		
		public RecordEntitySyntaxType(RecordDeclarationSyntax declaration)
		{
			_declaration = declaration;
		}

		public ClassKind Kind => ClassKind.Record;

		public BaseTypeDeclarationSyntax Declaration => _declaration;

		public SyntaxTree SyntaxTree => _declaration.SyntaxTree;

		public SyntaxToken Identifier => _declaration.Identifier;

		public SyntaxTokenList Modifiers => _declaration.Modifiers;

		public SyntaxList<AttributeListSyntax> AttributeLists => _declaration.AttributeLists;

		public EntityMetadata ToEntityMetadata(SemanticModel model)
		{
			return EntityMetadataFactory.Create(model, this);
		}

		public IEnumerable<SyntaxNode> GetChildNodes()
		{
			return _declaration.ChildNodes();
		}
	}
}
