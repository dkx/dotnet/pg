﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace DKX.Pg.AOT.Types
{
	public static class TypeInfoFactory
	{
		public static TypeInfo Create(SemanticModel model, PropertyDeclarationSyntax property)
		{
			var type = model.GetTypeInfo(property.Type).Type;

			if (type == null)
			{
				return null;
			}

			var isNullable = false;

			if (property.Type is NullableTypeSyntax nullableType)
			{
				isNullable = true;
				type = model.GetTypeInfo(nullableType.ElementType).Type;

				if (type == null)
				{
					return null;
				}
			}

			var dataRecordName = GetDataRecordName(type, out var mustUnbox, out var includeGlobal);
			var fullName = (includeGlobal ? "global::" : "") + type;

			return new TypeInfo(fullName, isNullable, mustUnbox, dataRecordName);
		}

		private static string GetDataRecordName(ITypeSymbol type, out bool mustUnbox, out bool includeGlobal)
		{
			var typeStr = type.ToString();

			mustUnbox = false;
			includeGlobal = false;

			if (typeStr == "bool")
			{
				return "GetBoolean";
			}

			if (typeStr == "byte")
			{
				return "GetByte";
			}

			if (typeStr == "char")
			{
				return "GetChar";
			}

			if (typeStr == "System.DateTime")
			{
				includeGlobal = true;
				return "GetDateTime";
			}

			if (typeStr == "decimal")
			{
				return "GetDecimal";
			}

			if (typeStr == "double")
			{
				return "GetDouble";
			}

			if (typeStr == "float")
			{
				return "GetFloat";
			}

			if (typeStr == "System.Guid")
			{
				includeGlobal = true;
				return "GetGuid";
			}

			if (typeStr == "short")
			{
				return "GetInt16";
			}

			if (typeStr == "int")
			{
				return "GetInt32";
			}

			if (typeStr == "long")
			{
				return "GetInt64";
			}

			if (typeStr == "string")
			{
				return "GetString";
			}

			if (typeStr == "object")
			{
				return "GetValue";
			}

			if (type.TypeKind == TypeKind.Array)
			{
				mustUnbox = true;
				return "GetValue";
			}

			if (type.TypeKind == TypeKind.Enum || type.TypeKind == TypeKind.Interface || type.TypeKind == TypeKind.Class)
			{
				mustUnbox = true;
				includeGlobal = true;
				return "GetValue";
			}

			mustUnbox = true;
			return "GetValue";
		}
	}
}
