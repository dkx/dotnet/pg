﻿namespace DKX.Pg.AOT.Types
{
	public sealed class TypeInfo
	{
		public TypeInfo(string fullName, bool isNullable, bool mustUnbox, string dataRecordGetter)
		{
			FullName = fullName;
			IsNullable = isNullable;
			MustUnbox = mustUnbox;
			DataRecordGetter = dataRecordGetter;
		}

		public string FullName { get; }

		public bool IsNullable { get; }
		
		public bool MustUnbox { get; }
		
		public string DataRecordGetter { get; }
	}
}
