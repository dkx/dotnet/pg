## Release 2.0.0

### New Rules

Rule ID | Category | Severity | Notes
--------|----------|----------|--------------------
DKXPG0001 | DKX.Pg.AOT | Error | Error loading class info
DKXPG0002 | DKX.Pg.AOT | Error | Entity must be partial
DKXPG0003 | DKX.Pg.AOT | Error | Entity must be public
DKXPG0004 | DKX.Pg.AOT | Error | Mapping data to non-entity type
DKXPG0005 | DKX.Pg.AOT | Warning | Mapping data to generic type
