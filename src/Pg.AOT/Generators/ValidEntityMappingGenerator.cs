﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace DKX.Pg.AOT.Generators
{
	[Generator]
	public sealed class ValidEntityMappingGenerator : ISourceGenerator
	{
		public void Initialize(GeneratorInitializationContext context)
		{
			context.RegisterForSyntaxNotifications(() => new SyntaxReceiver());
		}

		public void Execute(GeneratorExecutionContext context)
		{
			if (!(context.SyntaxReceiver is SyntaxReceiver syntaxReceiver))
			{
				return;
			}

			var models = new Dictionary<SyntaxTree, SemanticModel>();

			foreach (var invocation in syntaxReceiver.Invocations)
			{
				if (!models.TryGetValue(invocation.SyntaxTree, out var model))
				{
					model = context.Compilation.GetSemanticModel(invocation.SyntaxTree);
					models.Add(invocation.SyntaxTree, model);
				}

				var symbol = model.GetSymbolInfo(invocation).Symbol;
				if (symbol is IMethodSymbol method && method.ContainingType.ToString() == "DKX.Pg.Queries.IQuery")
				{
					var entityType = method.TypeArguments[0];
					if (entityType.TypeKind == TypeKind.TypeParameter)
					{
						context.ReportDiagnostic(Diagnostic.Create(DiagnosticDescriptors.GenericType, invocation.GetLocation()));
						continue;
					}

					if (
						entityType.TypeKind != TypeKind.Class ||
						!entityType.GetAttributes().Any(a => a.ToString() == "DKX.Pg.Entities.EntityAttribute")
					)
					{
						context.ReportDiagnostic(Diagnostic.Create(DiagnosticDescriptors.InvalidEntity, invocation.GetLocation(), entityType.ToString()));
					}
				}
			}
		}

		private sealed class SyntaxReceiver : ISyntaxReceiver
		{
			public readonly IList<InvocationExpressionSyntax> Invocations = new List<InvocationExpressionSyntax>();

			public void OnVisitSyntaxNode(SyntaxNode syntaxNode)
			{
				if (
					syntaxNode is InvocationExpressionSyntax ies &&
					ies.Expression is MemberAccessExpressionSyntax expr &&
					expr.Name is GenericNameSyntax name &&
					name.TypeArgumentList.Arguments.Count == 1
				)
				{
					var methodName = name.Identifier.ToString();
					if (methodName == "GetResult" || methodName == "GetSingleOrNullResult" || methodName == "GetSingleResult")
					{
						Invocations.Add(ies);
					}
				}
			}
		}
	}
}
