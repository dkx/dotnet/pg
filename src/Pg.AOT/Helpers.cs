﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace DKX.Pg.AOT
{
	public static class Helpers
	{
		public static string ToUnderscore(string name)
		{
			var regex = new Regex(@"([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)");
			var matches = regex.Matches(name);
			var parts = new List<string>();

			foreach (Match match in matches)
			{
				parts.Add(match.Value.ToLowerInvariant());
			}

			return string.Join("_", parts);
		}
	}
}
