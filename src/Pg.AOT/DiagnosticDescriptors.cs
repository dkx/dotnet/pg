﻿using Microsoft.CodeAnalysis;

namespace DKX.Pg.AOT
{
	public static class DiagnosticDescriptors
	{
		public static readonly DiagnosticDescriptor ErrorLoadingClassInfo = new DiagnosticDescriptor(
			id: "DKXPG0001",
			title: "Error loading class info",
			messageFormat: "Could not load class info for '{0}'",
			category: "DKX.Pg.AOT",
			defaultSeverity: DiagnosticSeverity.Error,
			isEnabledByDefault: true
		);
		
		public static readonly DiagnosticDescriptor EntityNotPartial = new DiagnosticDescriptor(
			id: "DKXPG0002",
			title: "Entity must be partial",
			messageFormat: "Entity class '{0}.{1}' must be marked as partial",
			category: "DKX.Pg.AOT",
			defaultSeverity: DiagnosticSeverity.Error,
			isEnabledByDefault: true
		);
		
		public static readonly DiagnosticDescriptor EntityNotPublic = new DiagnosticDescriptor(
			id: "DKXPG0003",
			title: "Entity must be public",
			messageFormat: "Entity class '{0}.{1}' must be marked as public",
			category: "DKX.Pg.AOT",
			defaultSeverity: DiagnosticSeverity.Error,
			isEnabledByDefault: true
		);
		
		public static readonly DiagnosticDescriptor InvalidEntity = new DiagnosticDescriptor(
			id: "DKXPG0004",
			title: "Mapping data to non-entity type",
			messageFormat: "Could not map data into '{0}', target type must be a partial class/record with [Entity] attribute",
			category: "DKX.Pg.AOT",
			defaultSeverity: DiagnosticSeverity.Error,
			isEnabledByDefault: true
		);
		
		public static readonly DiagnosticDescriptor GenericType = new DiagnosticDescriptor(
			id: "DKXPG0005",
			title: "Mapping data to generic type",
			messageFormat: "Could not analyze mapping to a generic type",
			category: "DKX.Pg.AOT",
			defaultSeverity: DiagnosticSeverity.Warning,
			isEnabledByDefault: true
		);
	}
}
