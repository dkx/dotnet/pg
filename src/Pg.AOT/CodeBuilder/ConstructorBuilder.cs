﻿using System.Collections.Generic;
using System.Text;

namespace DKX.Pg.AOT.CodeBuilder
{
	public sealed class ConstructorBuilder : BlocksContainer, ICodeBlock
	{
		private readonly string _className;

		private readonly IList<string> _parameters = new List<string>();

		public ConstructorBuilder(string className)
		{
			_className = className;
		}

		public void AddParameter(string type, string name)
		{
			_parameters.Add($"{type} {name}");
		}

		public string ToCode()
		{
			var addParameters = _parameters.Count > 0 ? string.Join(", ", _parameters) : "";
			var str = new StringBuilder();
			
			str.Append($"public {_className}({addParameters})\n");
			str.Append("{\n");
			str.Append(Blocks.ToCode(indent: true, appendNewLine: true));
			str.Append("}");

			return str.ToString();
		}
	}
}
