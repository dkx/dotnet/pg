﻿using System.Collections.Generic;
using System.Text;

namespace DKX.Pg.AOT.CodeBuilder
{
	public sealed class PropertyBuilder : ICodeBlock
	{
		private readonly string _type;

		private readonly string _name;

		private ICodeBlock _value;

		private readonly IList<string> _modifiers = new List<string>();

		public PropertyBuilder(string type, string name)
		{
			_type = type;
			_name = name;
		}

		public void AddModifier(string modifier)
		{
			_modifiers.Add(modifier);
		}

		public void SetValue(ICodeBlock value)
		{
			_value = value;
		}
		
		public string ToCode()
		{
			var str = new StringBuilder();

			if (_modifiers.Count > 0)
			{
				str.Append(string.Join(" ", _modifiers));
				str.Append(" ");
			}

			str.Append(_type);
			str.Append(" ");
			str.Append(_name);
			str.Append(" { get; }");

			if (_value != null)
			{
				str.Append(" = ");
				str.Append(_value.ToCode());
			}

			str.Append(";");

			return str.ToString();
		}
	}
}
