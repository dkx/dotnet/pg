﻿namespace DKX.Pg.AOT.CodeBuilder
{
	public interface ICodeBlock
	{
		string ToCode();
	}
}
