﻿using System.Collections.Generic;
using System.Text;

namespace DKX.Pg.AOT.CodeBuilder
{
	public sealed class MethodBuilder : BlocksContainer, ICodeBlock
	{
		private readonly string _name;

		private readonly string _returnType;

		private readonly IList<string> _parameters = new List<string>();

		public MethodBuilder(string name, string returnType)
		{
			_name = name;
			_returnType = returnType;
		}

		public void AddParameter(string type, string name)
		{
			_parameters.Add($"{type} {name}");
		}

		public string ToCode()
		{
			var addParameters = _parameters.Count > 0 ? string.Join(", ", _parameters) : "";

			var str = new StringBuilder();
			
			str.Append($"public {_returnType} {_name}({addParameters})\n");
			str.Append("{\n");
			str.Append(Blocks.ToCode(indent: true, appendNewLine: true));
			str.Append("}");

			return str.ToString();
		}
	}
}
