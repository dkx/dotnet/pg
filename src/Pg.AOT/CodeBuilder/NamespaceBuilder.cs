﻿using System.Text;

namespace DKX.Pg.AOT.CodeBuilder
{
	public sealed class NamespaceBuilder : BlocksContainer, ICodeBlock
	{
		private readonly string _name;
		
		public NamespaceBuilder(string name)
		{
			_name = name;
		}

		public string ToCode()
		{
			var str = new StringBuilder();

			str.Append($"namespace {_name}\n");
			str.Append("{\n");
			str.Append(Blocks.ToCode(indent: true, appendNewLine: true));
			str.Append("}");

			return str.ToString();
		}
	}
}
