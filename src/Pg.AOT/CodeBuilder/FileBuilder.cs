﻿using System.Text;
using Microsoft.CodeAnalysis.Text;

namespace DKX.Pg.AOT.CodeBuilder
{
	public sealed class FileBuilder : BlocksContainer
	{
		public SourceText ToSourceText()
		{
			return SourceText.From(Blocks.ToCode() + "\n", Encoding.UTF8);
		}
	}
}
