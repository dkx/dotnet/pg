﻿namespace DKX.Pg.AOT.CodeBuilder
{
	public sealed class RawCodeBuilder : ICodeBlock
	{
		private readonly string _code;
		
		public RawCodeBuilder(string code)
		{
			_code = code;
		}

		public string ToCode()
		{
			return _code;
		}
	}
}
