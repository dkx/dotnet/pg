﻿namespace DKX.Pg.AOT.CodeBuilder
{
	public sealed class ReturnBuilder : ICodeBlock
	{
		private readonly ICodeBlock _block;

		public ReturnBuilder(ICodeBlock block)
		{
			_block = block;
		}
		
		public string ToCode()
		{
			return $"return {_block.ToCode()};";
		}
	}
}
