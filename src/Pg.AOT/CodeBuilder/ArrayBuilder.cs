﻿using System.Text;

namespace DKX.Pg.AOT.CodeBuilder
{
	public sealed class ArrayBuilder : BlocksContainer, ICodeBlock
	{
		private readonly string _type;

		public ArrayBuilder(string type)
		{
			_type = type;
		}

		public string ToCode()
		{
			if (Blocks.IsEmpty())
			{
				return $"global::System.Array.Empty<{_type}>()";
			}

			var str = new StringBuilder();

			str.Append($"new {_type}[]\n");
			str.Append("{\n");
			str.Append(Blocks.ToCode(indent: true, appendNewLine: true));
			str.Append("}");

			return str.ToString();
		}
	}
}
