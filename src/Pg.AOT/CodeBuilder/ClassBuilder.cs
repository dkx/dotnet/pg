﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DKX.Pg.AOT.CodeBuilder
{
	public sealed class ClassBuilder : BlocksContainer, ICodeBlock
	{
		private readonly ClassKind _kind;
		
		private readonly string _name;

		private readonly IList<string> _modifiers = new List<string>();

		private readonly IList<string> _extendOrImplement = new List<string>();

		public ClassBuilder(ClassKind kind, string name)
		{
			_kind = kind;
			_name = name;
		}

		public void AddModifier(string modifier)
		{
			_modifiers.Add(modifier);
		}

		public void AddExtendOrImplement(string extendOrImplement)
		{
			_extendOrImplement.Add(extendOrImplement);
		}

		public string ToCode()
		{
			var str = new StringBuilder();

			if (_modifiers.Count > 0)
			{
				str.Append(string.Join(" ", _modifiers));
				str.Append(" ");
			}

			switch (_kind)
			{
				case ClassKind.Class:
					str.Append("class");
					break;
				case ClassKind.Record:
					str.Append("record");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			str.Append(" ");
			str.Append(_name);

			if (_extendOrImplement.Count > 0)
			{
				str.Append(" : ");
				str.Append(string.Join(", ", _extendOrImplement));
			}

			str.Append($"\n");
			str.Append("{\n");
			str.Append(Blocks.ToCode(indent: true, appendNewLine: true));
			str.Append("}");

			return str.ToString();
		}
	}
}
