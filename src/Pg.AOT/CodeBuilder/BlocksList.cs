﻿using System.Collections.Generic;
using System.Linq;

namespace DKX.Pg.AOT.CodeBuilder
{
	public sealed class BlocksList
	{
		private readonly IList<ICodeBlock> _blocks = new List<ICodeBlock>();

		public void Add(ICodeBlock block)
		{
			_blocks.Add(block);
		}

		public bool IsEmpty()
		{
			return _blocks.Count == 0;
		}

		public string ToCode(bool indent = false, bool appendNewLine = false)
		{
			var code = string.Join("\n", _blocks.Select(b => b.ToCode()));

			if (indent)
			{
				var parts = code.Split('\n');
				var indented = parts.Select(p => p == "" ? "" : $"\t{p}");
				code = string.Join("\n", indented);
			}

			if (appendNewLine && _blocks.Count > 0)
			{
				code += "\n";
			}

			return code;
		}
	}
}
