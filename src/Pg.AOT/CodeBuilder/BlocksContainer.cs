﻿namespace DKX.Pg.AOT.CodeBuilder
{
	public abstract class BlocksContainer
	{
		protected readonly BlocksList Blocks = new BlocksList();

		public void Add(ICodeBlock block)
		{
			Blocks.Add(block);
		}

		public void Add(string code)
		{
			Add(new RawCodeBuilder(code));
		}
	}
}
