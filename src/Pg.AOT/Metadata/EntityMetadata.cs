﻿using DKX.Pg.AOT.CodeBuilder;

namespace DKX.Pg.AOT.Metadata
{
	internal sealed class EntityMetadata
	{
		public EntityMetadata(
			ClassKind kind,
			string containingNamespace,
			string name,
			PropertyMetadata[] properties
		)
		{
			Kind = kind;
			ContainingNamespace = containingNamespace;
			Name = name;
			Properties = properties;
		}
		
		public ClassKind Kind { get; }
		
		public string ContainingNamespace { get; }
		
		public string Name { get; }
		
		public PropertyMetadata[] Properties { get; }

		public string FullName => ContainingNamespace + "." + Name;
	}
}
