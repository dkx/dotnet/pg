﻿using TypeInfo = DKX.Pg.AOT.Types.TypeInfo;

namespace DKX.Pg.AOT.Metadata
{
	public sealed class PropertyMetadata
	{
		public PropertyMetadata(string name, string databaseName, TypeInfo typeInfo, bool isIdentifier, bool isGenerated)
		{
			Name = name;
			DatabaseName = databaseName;
			TypeInfo = typeInfo;
			IsIdentifier = isIdentifier;
			IsGenerated = isGenerated;
		}
		
		public string Name { get; }
		
		public string DatabaseName { get; }
		
		public TypeInfo TypeInfo { get; }
		
		public bool IsIdentifier { get; }
		
		public bool IsGenerated { get; }
	}
}
