﻿using System.Linq;
using DKX.Pg.AOT.Entities;
using DKX.Pg.AOT.Exceptions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace DKX.Pg.AOT.Metadata
{
	internal static class EntityMetadataFactory
	{
		public static EntityMetadata Create(SemanticModel model, IEntitySyntaxType entity)
		{
			var classType = model.GetDeclaredSymbol(entity.Declaration);
			if (classType == null)
			{
				throw new CouldNotLoadClassInfoException(entity.Identifier, Location.Create(entity.SyntaxTree, entity.Identifier.Span));
			}
			
			if (!entity.Modifiers.Any(SyntaxKind.PublicKeyword))
			{
				throw new EntityIsNotPublicException(classType, Location.Create(entity.SyntaxTree, entity.Identifier.Span));
			}
			
			if (!entity.Modifiers.Any(SyntaxKind.PartialKeyword))
			{
				throw new EntityIsNotPartialException(classType, Location.Create(entity.SyntaxTree, entity.Identifier.Span));
			}
			
			var properties = entity.GetChildNodes()
				.Where(n => n is PropertyDeclarationSyntax)
				.Cast<PropertyDeclarationSyntax>()
				.Where(p => p.AttributeLists.SelectMany(l => l.Attributes).All(a => model.GetTypeInfo(a).Type?.ToString() != "DKX.Pg.Entities.NotMappedAttribute"))
				.Select(property => PropertyMetadataFactory.Create(model, property))
				.Where(entityProperty => entityProperty != null)
				.ToArray();

			return new EntityMetadata(entity.Kind, classType.ContainingNamespace.ToString(), classType.Name, properties);
		}
	}
}
