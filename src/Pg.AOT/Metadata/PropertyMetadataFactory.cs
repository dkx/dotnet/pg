﻿using System.Linq;
using DKX.Pg.AOT.Types;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace DKX.Pg.AOT.Metadata
{
	public static class PropertyMetadataFactory
	{
		public static PropertyMetadata Create(SemanticModel model, PropertyDeclarationSyntax property)
		{
			var typeInfo = TypeInfoFactory.Create(model, property);
			if (typeInfo == null)
			{
				return null;
			}

			var name = property.Identifier.ToString();
			var databaseName = Helpers.ToUnderscore(name);

			var nameAttribute = property.AttributeLists
				.SelectMany(l => l.Attributes)
				.FirstOrDefault(a => model.GetTypeInfo(a).Type?.ToString() == "DKX.Pg.Entities.NameAttribute");

			if (
				nameAttribute?.ArgumentList != null &&
				nameAttribute.ArgumentList.Arguments.Count == 1 &&
				nameAttribute.ArgumentList.Arguments[0].Expression is LiteralExpressionSyntax nameAttributeArg &&
				nameAttributeArg.IsKind(SyntaxKind.StringLiteralExpression)
			)
			{
				databaseName = nameAttributeArg.Token.ValueText;
			}
			
			var idAttribute = property.AttributeLists
				.SelectMany(l => l.Attributes)
				.FirstOrDefault(a => model.GetTypeInfo(a).Type?.ToString() == "DKX.Pg.Entities.IdAttribute");
			
			var generatedAttribute = property.AttributeLists
				.SelectMany(l => l.Attributes)
				.FirstOrDefault(a => model.GetTypeInfo(a).Type?.ToString() == "DKX.Pg.Entities.GeneratedAttribute");

			return new PropertyMetadata(name, databaseName, typeInfo, idAttribute != null, generatedAttribute != null);
		}
	}
}
