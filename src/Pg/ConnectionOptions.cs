using DKX.Pg.Connections;
using DKX.Pg.Transactions;

namespace DKX.Pg
{
	public sealed class ConnectionOptions
	{
		public bool? AutoOpen { get; set; }

		public bool? UseLegacyNamedParameters { get; set; }

		public bool? DisposeNativeConnection { get; set; }
		
		public bool? PrepareCommands { get; set; }
		
		public IAutoConnector? AutoConnector { get; set; }
		
		public ITransactionsManager? TransactionsManager { get; set; }
	}
}
