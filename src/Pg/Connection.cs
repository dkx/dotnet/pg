﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using DKX.Pg.Connections;
using DKX.Pg.Events;
using DKX.Pg.Exceptions;
using DKX.Pg.Metadata;
using DKX.Pg.Parsing;
using DKX.Pg.Queries;
using DKX.Pg.Snippets;
using DKX.Pg.Transactions;
using Npgsql;

namespace DKX.Pg
{
	public sealed class Connection : IConnection
	{
		private readonly INativeConnectionProvider _connectionProvider;

		private readonly IEntitiesMetadataRegistry _metadataRegistry;

		private readonly IAutoConnector _autoConnector;

		private readonly ITransactionsManager _transactionsManager;

		private readonly IEventListenersManager _events;

		private readonly ISqlParser _parser;

		private readonly bool _useLegacyNamedParameters;

		private readonly bool _disposeNativeConnection;

		private readonly bool _prepareCommands;

		private Transaction? _transaction;

		public Connection(INativeConnectionProvider nativeConnectionProvider, IEntitiesMetadataProvider metadataProvider, ConnectionOptions? options = null)
		{
			_connectionProvider = nativeConnectionProvider;
			_metadataRegistry = new EntitiesMetadataRegistry(metadataProvider);
			_events = new EventListenersManager();
			_parser = new SqlParser();
			_autoConnector = options?.AutoConnector ?? new DefaultAutoConnector();
			_transactionsManager = options?.TransactionsManager ?? new DefaultTransactionsManager();
			_useLegacyNamedParameters = options?.UseLegacyNamedParameters ?? false;
			_disposeNativeConnection = options?.DisposeNativeConnection ?? true;
			_prepareCommands = options?.PrepareCommands ?? true;
		}

		public Connection(NpgsqlConnection connection, IEntitiesMetadataProvider metadataProvider, ConnectionOptions? options = null)
			: this(new DefaultNativeConnectionProvider(connection), metadataProvider, options)
		{
		}

		public void Dispose()
		{
			if (_disposeNativeConnection)
			{
				_connectionProvider.GetConnection().Dispose();
			}
		}

		public async ValueTask DisposeAsync()
		{
			if (_disposeNativeConnection)
			{
				await _connectionProvider.GetConnection().DisposeAsync();
			}
		}

		public NpgsqlConnection NativeConnection => _connectionProvider.GetConnection();

		public void AddEventListener(EventListener listener)
		{
			_events.Add(listener);
		}

		public Task TryOpen(CancellationToken cancellationToken = default)
		{
			return _autoConnector.Open(_connectionProvider.GetConnection(), cancellationToken);
		}

		public Task Open(CancellationToken cancellationToken = default)
		{
			return _connectionProvider.GetConnection().OpenAsync(cancellationToken);
		}

		public Task Close()
		{
			return _connectionProvider.GetConnection().CloseAsync();
		}

		public IQuery Query(FormattableString sql, CommandType commandType = CommandType.Text)
		{
			var ctx = new QueryParsingContext(_parser, _metadataRegistry, _useLegacyNamedParameters);
			var parsed = _parser.Parse(sql, ctx);
			return new Query(_connectionProvider, _autoConnector, _transactionsManager, _events, _metadataRegistry, _prepareCommands, commandType, parsed, ctx.Parameters.ToImmutableList());
		}

		public IQuery QueryRaw(string sql, CommandType commandType = CommandType.Text)
		{
			return new Query(_connectionProvider, _autoConnector, _transactionsManager, _events, _metadataRegistry, _prepareCommands, commandType, sql, ImmutableArray<DbParameter>.Empty);
		}

		internal IQuery CreateInsertQuery<T>(string table, T data)
			where T : class
		{
			return Query($"INSERT INTO {new RawSnippet(_parser.Quote(table))} {new ValuesSnippet<T>(data)}");
		}

		public Task<int> Insert<T>(string table, T data, CancellationToken cancellationToken = default)
			where T : class
		{
			return CreateInsertQuery(table, data).Execute(cancellationToken);
		}

		internal IQuery CreateUpdateQuery<T>(string table, T data)
			where T : class
		{
			var metadata = _metadataRegistry.GetMetadata<T>();
			var where = new List<FormattableString>();
			
			foreach (var property in metadata.Properties)
			{
				if (property.IsIdentifier)
				{
					where.Add($"{new RawSnippet(_parser.Quote(property.DatabaseName))} = {property.GetValue(data)}");	// implicit parameter
				}
			}

			if (where.Count == 0)
			{
				throw new QueryException($"Can not update entity {typeof(T)} because it does not have any ids");
			}
			
			return Query($"UPDATE {new RawSnippet(_parser.Quote(table))} SET {new SetSnippet<T>(data)} WHERE {new CompositeSnippet(CompositeSnippetType.And, where)}");
		}

		public Task<int> Update<T>(string table, T data, CancellationToken cancellationToken = default)
			where T : class
		{
			return CreateUpdateQuery(table, data).Execute(cancellationToken);
		}
		
		public async Task<ITransaction> BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified, CancellationToken cancellationToken = default)
		{
			if (_transactionsManager.GetCurrentNativeTransaction() is { } currentNativeTransaction)
			{
				if (_transaction is not null && _transaction.NativeTransaction != currentNativeTransaction)
				{
					throw new InvalidStateException("Can not begin new transaction when there is already a different transaction running");
				}
			}

			var connection = _connectionProvider.GetConnection();
			
			await _autoConnector.Open(connection, cancellationToken);

			if (_transaction != null)
			{
				return await _transaction.CreateSavepoint(cancellationToken);
			}

			var nativeTransaction = await connection.BeginTransactionAsync(isolationLevel, cancellationToken);

			_transactionsManager.SetCurrentNativeTransaction(nativeTransaction);
			_transaction = new Transaction(nativeTransaction);
			_transaction.Closed += (sender, args) =>
			{
				_transaction = null;
				_transactionsManager.ClearCurrentNativeTransaction();
			};

			return _transaction;
		}

		public async Task Transactional(Func<Task> fn, IsolationLevel isolationLevel = IsolationLevel.Unspecified, CancellationToken cancellationToken = default)
		{
			await using var transaction = await BeginTransaction(isolationLevel, cancellationToken);

			try
			{
				await fn();
				await transaction.Commit(cancellationToken);
			}
			catch (Exception)
			{
				await transaction.Rollback(cancellationToken);
				throw;
			}
		}

		public async Task<T> Transactional<T>(Func<Task<T>> fn, IsolationLevel isolationLevel = IsolationLevel.Unspecified, CancellationToken cancellationToken = default)
		{
			await using var transaction = await BeginTransaction(isolationLevel, cancellationToken);

			try
			{
				var result = await fn();
				await transaction.Commit(cancellationToken);
				return result;
			}
			catch (Exception)
			{
				await transaction.Rollback(cancellationToken);
				throw;
			}
		}

		public IEntityMetadata<T> LoadEntityMetadata<T>()
			where T : class
		{
			return _metadataRegistry.GetMetadata<T>();
		}

		public string Quote(string str)
		{
			return _parser.Quote(str);
		}
	}
}
