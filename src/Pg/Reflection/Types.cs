﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

namespace DKX.Pg.Reflection
{
	internal static class Types
	{
		public static (Type RealType, bool IsNullable) GetRealType(PropertyInfo property)
		{
			return GetRealType(property.PropertyType, property.DeclaringType, property.CustomAttributes);
		}

		public static (Type RealType, bool IsNullable) GetRealType(ParameterInfo parameter)
		{
			return GetRealType(parameter.ParameterType, parameter.Member, parameter.CustomAttributes);
		}
		
		private static (Type RealType, bool IsNullable) GetRealType(Type memberType, MemberInfo? declaringType, IEnumerable<CustomAttributeData> customAttributes)
		{
			// https://gitlab.com/dkx/dotnet/pg/-/issues/11
			if (memberType == typeof(string))
			{
				return (memberType, true);
			}

			if (memberType.IsClass || memberType.IsInterface)
			{
				return (memberType, CheckAttributes(declaringType, customAttributes));
			}
			
			return SimpleCheck(memberType);
		}

		private static (Type RealType, bool IsNullable) SimpleCheck(Type type)
		{
			var nullableType = Nullable.GetUnderlyingType(type);
			if (nullableType != null)
			{
				return (nullableType, true);
			}

			return (type, false);
		}

		/*
		 * https://stackoverflow.com/questions/58453972/how-to-use-net-reflection-to-check-for-nullable-reference-type/58454489#58454489
		 */
		private static bool CheckAttributes(MemberInfo? declaringType, IEnumerable<CustomAttributeData> customAttributes)
		{
			var nullable = customAttributes.FirstOrDefault(a => a.AttributeType.FullName == "System.Runtime.CompilerServices.NullableAttribute");
			if (nullable != null && nullable.ConstructorArguments.Count == 1)
			{
				var attributeArgument = nullable.ConstructorArguments[0];
				if (attributeArgument.ArgumentType == typeof(byte[]))
				{
					var args = (ReadOnlyCollection<CustomAttributeTypedArgument>) attributeArgument.Value!;
					if (args.Count > 0 && args[0].ArgumentType == typeof(byte))
					{
						return (byte)args[0].Value! == 2;
					}
				}
				else if (attributeArgument.ArgumentType == typeof(byte))
				{
					return (byte)attributeArgument.Value! == 2;
				}
			}
			
			for (var type = declaringType; type != null; type = type.DeclaringType)
			{
				var context = type.CustomAttributes.FirstOrDefault(x => x.AttributeType.FullName == "System.Runtime.CompilerServices.NullableContextAttribute");
				if (
					context != null &&
					context.ConstructorArguments.Count == 1 &&
					context.ConstructorArguments[0].ArgumentType == typeof(byte)
				)
				{
					return (byte)context.ConstructorArguments[0].Value! == 2;
				}
			}

			return false;
		}
	}
}
