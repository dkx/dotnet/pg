﻿using System;

namespace DKX.Pg.Parsing
{
	internal sealed class SqlParser : ISqlParser
	{
		public string Parse(FormattableString sql, IQueryParsingContext ctx)
		{
			return sql.ToString(new SqlFormatProvider(this, ctx));
		}

		public string Quote(string str)
		{
			return "\"" + str + "\"";
		}
	}
}
