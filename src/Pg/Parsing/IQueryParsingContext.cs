﻿using System;
using DKX.Pg.Metadata;

namespace DKX.Pg.Parsing
{
	public interface IQueryParsingContext
	{
		string AddParameter(object? value);

		string AddParameter<T>(T value);

		string ParseSqlPart(FormattableString part);

		string Quote(string str);

		IEntityMetadata<T> LoadEntityMetadata<T>()
			where T : class;
	}
}
