﻿using System;

namespace DKX.Pg.Parsing
{
	internal sealed class SqlFormatProvider : IFormatProvider
	{
		private readonly SqlFormatter _formatter;

		public SqlFormatProvider(ISqlParser parser, IQueryParsingContext ctx)
		{
			_formatter = new SqlFormatter(parser, ctx);
		}
		
		public object? GetFormat(Type? formatType)
		{
			if (formatType == typeof(ICustomFormatter))
			{
				return _formatter;
			}

			return null;
		}
	}
}
