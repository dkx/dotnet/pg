﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using DKX.Pg.Metadata;
using Npgsql;

namespace DKX.Pg.Parsing
{
	internal sealed class QueryParsingContext : IQueryParsingContext
	{
		private readonly ISqlParser _parser;

		private readonly IEntitiesMetadataRegistry _metadataRegistry;

		private readonly bool _useLegacyNamedParameters;
		
		private readonly List<DbParameter> _parameters = new List<DbParameter>();

		public QueryParsingContext(ISqlParser parser, IEntitiesMetadataRegistry metadataRegistry, bool useLegacyNamedParameters)
		{
			_parser = parser;
			_metadataRegistry = metadataRegistry;
			_useLegacyNamedParameters = useLegacyNamedParameters;
		}

		public string AddParameter(object? value)
		{
			if (_useLegacyNamedParameters)
			{
				var name = "p" + _parameters.Count;
				_parameters.Add(new NpgsqlParameter(name, value ?? DBNull.Value));
				return "@" + name;
			}

			_parameters.Add(new NpgsqlParameter(null, value ?? DBNull.Value));
			return "$" + _parameters.Count;
		}

		public string AddParameter<T>(T value)
		{
			if (_useLegacyNamedParameters)
			{
				var name = "p" + _parameters.Count;
				_parameters.Add(value == null ? new NpgsqlParameter(name, DBNull.Value) : new NpgsqlParameter<T>(name, value));
				return "@" + name;
			}

			_parameters.Add(value == null ? new NpgsqlParameter(null, DBNull.Value) : new NpgsqlParameter<T>(null, value));
			return "$" + _parameters.Count;
		}

		public string ParseSqlPart(FormattableString part)
		{
			return _parser.Parse(part, this);
		}

		public string Quote(string str)
		{
			return _parser.Quote(str);
		}

		public IEntityMetadata<T> LoadEntityMetadata<T>()
			where T : class
		{
			return _metadataRegistry.GetMetadata<T>();
		}

		internal IReadOnlyCollection<DbParameter> Parameters => _parameters;
	}
}
