﻿using System;
using DKX.Pg.Snippets;

namespace DKX.Pg.Parsing
{
	internal sealed class SqlFormatter : ICustomFormatter
	{
		private readonly ISqlParser _parser;

		private readonly IQueryParsingContext _ctx;

		public SqlFormatter(ISqlParser parser, IQueryParsingContext ctx)
		{
			_parser = parser;
			_ctx = ctx;
		}

		public string Format(string? format, object? arg, IFormatProvider? formatProvider)
		{
			if (arg is FormattableString innerTemplate)
			{
				return _parser.Parse(innerTemplate, _ctx);
			}

			if (arg is ISnippet snippet)
			{
				return snippet.Process(_ctx, format);
			}

			return _ctx.AddParameter(arg);
		}
	}
}
