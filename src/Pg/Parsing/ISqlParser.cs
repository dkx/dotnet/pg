﻿using System;

namespace DKX.Pg.Parsing
{
	internal interface ISqlParser
	{
		string Parse(FormattableString sql, IQueryParsingContext ctx);

		string Quote(string str);
	}
}
