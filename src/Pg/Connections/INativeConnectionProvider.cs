﻿using Npgsql;

namespace DKX.Pg.Connections
{
	public interface INativeConnectionProvider
	{
		NpgsqlConnection GetConnection();
	}
}
