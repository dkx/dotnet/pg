using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.Pg.Connections
{
	public sealed class DefaultAutoConnector : IAutoConnector
	{
		public async Task Open(DbConnection connection, CancellationToken cancellationToken = default)
		{
			if (ShouldOpen(connection.State))
			{
				await connection.OpenAsync(cancellationToken);
			}
		}

		private static bool ShouldOpen(ConnectionState state)
		{
			if (
				state == ConnectionState.Open ||
				state == ConnectionState.Connecting ||
				state == (ConnectionState.Open | ConnectionState.Executing) ||
				state == (ConnectionState.Open | ConnectionState.Fetching)
			)
			{
				return false;
			}

			return true;
		}
	}
}
