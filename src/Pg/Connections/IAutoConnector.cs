using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.Pg.Connections
{
	public interface IAutoConnector
	{
		Task Open(DbConnection connection, CancellationToken cancellationToken = default);
	}
}
