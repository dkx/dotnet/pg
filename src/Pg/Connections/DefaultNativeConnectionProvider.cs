﻿using Npgsql;

namespace DKX.Pg.Connections
{
	public sealed class DefaultNativeConnectionProvider : INativeConnectionProvider
	{
		private readonly NpgsqlConnection _connection;

		public DefaultNativeConnectionProvider(NpgsqlConnection connection)
		{
			_connection = connection;
		}

		public NpgsqlConnection GetConnection()
		{
			return _connection;
		}
	}
}
