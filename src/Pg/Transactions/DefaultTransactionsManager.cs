﻿using System.Data.Common;

namespace DKX.Pg.Transactions
{
	public sealed class DefaultTransactionsManager : ITransactionsManager
	{
		private DbTransaction? _transaction;
		
		public DbTransaction? GetCurrentNativeTransaction()
		{
			return _transaction;
		}

		public void SetCurrentNativeTransaction(DbTransaction transaction)
		{
			_transaction = transaction;
		}

		public void ClearCurrentNativeTransaction()
		{
			_transaction = null;
		}
	}
}
