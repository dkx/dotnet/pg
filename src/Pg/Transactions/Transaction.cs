using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.Pg.Transactions
{
	internal sealed class Transaction : ITransaction
	{
		public event EventHandler? Closed;

		private readonly SavepointFactory _savepointFactory;

		public Transaction(DbTransaction transaction)
		{
			NativeTransaction = transaction;
			_savepointFactory = new SavepointFactory(transaction);
		}
		
		public DbTransaction NativeTransaction { get; }

		public void Dispose()
		{
			NativeTransaction.Dispose();
			Closed?.Invoke(this, EventArgs.Empty);
		}

		public async ValueTask DisposeAsync()
		{
			await NativeTransaction.DisposeAsync();
			Closed?.Invoke(this, EventArgs.Empty);
		}

		public Task Commit(CancellationToken cancellationToken = default)
		{
			return NativeTransaction.CommitAsync(cancellationToken);
		}

		public Task Rollback(CancellationToken cancellationToken = default)
		{
			return NativeTransaction.RollbackAsync(cancellationToken);
		}

		public async Task<ITransaction> CreateSavepoint(CancellationToken cancellationToken = default)
		{
			return await _savepointFactory.Create(cancellationToken);
		}
	}
}
