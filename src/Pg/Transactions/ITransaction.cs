﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.Pg.Transactions
{
	public interface ITransaction : IDisposable, IAsyncDisposable
	{
		Task Commit(CancellationToken cancellationToken = default);

		Task Rollback(CancellationToken cancellationToken = default);
	}
}
