using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.Pg.Transactions
{
	internal sealed class Savepoint : ITransaction
	{
		public event EventHandler? Closed;

		private readonly SavepointFactory _savepointFactory;
		
		private readonly string _name;

		private readonly DbTransaction _dbTransaction;

		public Savepoint(SavepointFactory savepointFactory, string name, DbTransaction dbTransaction)
		{
			_savepointFactory = savepointFactory;
			_name = name;
			_dbTransaction = dbTransaction;
		}
		
		public void Dispose()
		{
			Closed?.Invoke(this, EventArgs.Empty);
		}

		public ValueTask DisposeAsync()
		{
			Closed?.Invoke(this, EventArgs.Empty);
			return new ValueTask();
		}

		public Task Commit(CancellationToken cancellationToken = default)
		{
			return _dbTransaction.ReleaseAsync(_name, cancellationToken);
		}

		public Task Rollback(CancellationToken cancellationToken = default)
		{
			return _dbTransaction.RollbackAsync(_name, cancellationToken);
		}

		public Task<ITransaction> CreateSavepoint(CancellationToken cancellationToken = default)
		{
			return _savepointFactory.Create(cancellationToken);
		}
	}
}
