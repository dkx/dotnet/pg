﻿using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.Pg.Transactions
{
	internal sealed class SavepointFactory
	{
		private readonly DbTransaction _dbTransaction;

		private uint _savepointsCount;
		
		public SavepointFactory(DbTransaction dbTransaction)
		{
			_dbTransaction = dbTransaction;
		}
		
		public async Task<ITransaction> Create(CancellationToken cancellationToken)
		{
			var name = $"dkx_pg_savepoint_{_savepointsCount++}";
			await _dbTransaction.SaveAsync(name, cancellationToken);
			var savepoint = new Savepoint(this, name, _dbTransaction);

			savepoint.Closed += (sender, args) =>
			{
				_savepointsCount--;
			};

			return savepoint;
		}
	}
}
