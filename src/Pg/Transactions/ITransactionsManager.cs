﻿using System.Data.Common;

namespace DKX.Pg.Transactions
{
	public interface ITransactionsManager
	{
		DbTransaction? GetCurrentNativeTransaction();

		void SetCurrentNativeTransaction(DbTransaction transaction);

		void ClearCurrentNativeTransaction();
	}
}
