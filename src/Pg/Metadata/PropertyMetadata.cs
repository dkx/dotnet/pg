﻿using System;

namespace DKX.Pg.Metadata
{
	public sealed class PropertyMetadata<TEntity> : IPropertyMetadata<TEntity>
		where TEntity : class
	{
		private readonly Func<TEntity, object?> _valueGetter;

		public PropertyMetadata(string name, string databaseName, bool isIdentifier, bool isGenerated, Func<TEntity, object?> valueGetter)
		{
			Name = name;
			DatabaseName = databaseName;
			IsIdentifier = isIdentifier;
			IsGenerated = isGenerated;
			_valueGetter = valueGetter;
		}
		
		public string Name { get; }
		
		public string DatabaseName { get; }

		public bool IsIdentifier { get; }
		
		public bool IsGenerated { get; }
		
		public object? GetValue(TEntity entity)
		{
			return _valueGetter(entity);
		}
	}
}
