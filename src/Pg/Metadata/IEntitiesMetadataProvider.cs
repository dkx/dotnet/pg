﻿using System;
using System.Collections.Generic;

namespace DKX.Pg.Metadata
{
	public interface IEntitiesMetadataProvider
	{
		IEnumerable<Type> GetEntitiesMetadataTypes();
	}
}
