﻿namespace DKX.Pg.Metadata
{
	internal interface IEntitiesMetadataRegistry
	{
		IEntityMetadata<TEntity> GetMetadata<TEntity>()
			where TEntity : class;
	}
}
