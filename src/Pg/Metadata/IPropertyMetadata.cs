﻿namespace DKX.Pg.Metadata
{
	public interface IPropertyMetadata<in TEntity>
		where TEntity : class
	{
		string Name { get; }
		
		string DatabaseName { get; }
		
		bool IsIdentifier { get; }
		
		bool IsGenerated { get; }

		object? GetValue(TEntity entity);
	}
}
