﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DKX.Pg.Metadata
{
	public sealed class AssemblyEntitiesMetadataProvider : IEntitiesMetadataProvider
	{
		private readonly IEnumerable<Assembly> _assemblies;

		public AssemblyEntitiesMetadataProvider(IEnumerable<Assembly> assemblies)
		{
			_assemblies = assemblies;
		}

		public AssemblyEntitiesMetadataProvider(params Assembly[] assemblies)
		{
			_assemblies = assemblies;
		}

		public IEnumerable<Type> GetEntitiesMetadataTypes()
		{
			var metadataType = typeof(IEntityMetadata<>);

			return _assemblies
				.SelectMany(a => a.GetTypes())
				.Where(t => t.IsPublic && t.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == metadataType));
		}
	}
}
