﻿using System.Data;

namespace DKX.Pg.Metadata
{
	public interface IEntityMetadata
	{
	}
	
	public interface IEntityMetadata<TEntity> : IEntityMetadata
		where TEntity : class
	{
		IPropertyMetadata<TEntity>[] Properties { get; }
		
		int[] CreateMapping(IDataRecord record);

		TEntity CreateEntity(IDataRecord record, int[] mapping);
	}
}
