﻿using System;
using System.Collections.Generic;
using System.Linq;
using DKX.Pg.Entities;
using DKX.Pg.Exceptions;

namespace DKX.Pg.Metadata
{
	internal sealed class EntitiesMetadataRegistry : IEntitiesMetadataRegistry
	{
		private readonly IReadOnlyDictionary<Type, IEntityMetadata> _metadata;

		public EntitiesMetadataRegistry(IEntitiesMetadataProvider provider)
		{
			var metadataType = typeof(IEntityMetadata<>);
			var metadata = new Dictionary<Type, IEntityMetadata>();

			foreach (var type in provider.GetEntitiesMetadataTypes())
			{
				var metadataInterface = type.GetInterfaces().FirstOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == metadataType);
				if (metadataInterface == null)
				{
					continue;
				}

				metadata.Add(metadataInterface.GetGenericArguments()[0], (IEntityMetadata)Activator.CreateInstance(type)!);
			}

			_metadata = metadata;
		}

		public IEntityMetadata<TEntity> GetMetadata<TEntity>()
			where TEntity : class
		{
			var type = typeof(TEntity);
			
			if (_metadata.TryGetValue(type, out var metadata) && metadata is IEntityMetadata<TEntity> entityMetadata)
			{
				return entityMetadata;
			}

			if (Attribute.GetCustomAttribute(type, typeof(EntityAttribute)) == null)
			{
				throw new MetadataException($"Entity '{type}' is missing [Entity] class attribute");
			}

			throw new MetadataException($"Missing entity metadata for '{type}', do you have DKX.Pg.AOT installed correctly?");
		}
	}
}
