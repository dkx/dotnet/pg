﻿using System;

namespace DKX.Pg.Entities
{
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class IdAttribute : Attribute
	{
	}
}
