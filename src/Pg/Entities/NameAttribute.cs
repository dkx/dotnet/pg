﻿using System;

namespace DKX.Pg.Entities
{
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class NameAttribute : Attribute
	{
		public NameAttribute(string name)
		{
			Name = name;
		}
		
		public string Name { get; }
	}
}
