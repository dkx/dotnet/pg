﻿using System;

namespace DKX.Pg.Entities
{
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class EntityAttribute : Attribute
	{
	}
}
