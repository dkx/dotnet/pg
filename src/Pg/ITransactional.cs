﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using DKX.Pg.Transactions;

namespace DKX.Pg
{
	public interface ITransactional
	{
		Task<ITransaction> BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified, CancellationToken cancellationToken = default);

		Task Transactional(Func<Task> fn, IsolationLevel isolationLevel = IsolationLevel.Unspecified, CancellationToken cancellationToken = default);
		
		Task<T> Transactional<T>(Func<Task<T>> fn, IsolationLevel isolationLevel = IsolationLevel.Unspecified, CancellationToken cancellationToken = default);
	}
}
