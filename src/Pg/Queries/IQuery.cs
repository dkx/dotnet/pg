﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.Pg.Queries
{
	public interface IQuery
	{
		CommandType CommandType { get; }
		
		Guid Id { get; }
		
		string Sql { get; }
		
		IReadOnlyCollection<DbParameter> Parameters { get; }

		IQuery SetSql(string sql);

		IQuery SetPrepare(bool prepare);

		IQuery SetParameter(string name, object? value);

		IQuery SetParameter<T>(string name, T? value);

		IAsyncEnumerable<T> GetResult<T>(CancellationToken cancellationToken = default)
			where T : class;

		Task<T?> GetSingleOrNullResult<T>(CancellationToken cancellationToken = default)
			where T : class;

		Task<T> GetSingleResult<T>(CancellationToken cancellationToken = default)
			where T : class;

		IAsyncEnumerable<T> GetScalarResult<T>(CancellationToken cancellationToken = default);

		Task<T> GetSingleScalarResult<T>(CancellationToken cancellationToken = default);

		Task<int> Execute(CancellationToken cancellationToken = default);
	}
}
