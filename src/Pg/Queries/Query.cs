﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data;
using System.Data.Common;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using DKX.Pg.Connections;
using DKX.Pg.Events;
using DKX.Pg.Exceptions;
using DKX.Pg.Metadata;
using DKX.Pg.Transactions;
using Npgsql;

namespace DKX.Pg.Queries
{
	internal sealed class Query : IQuery
	{
		private readonly INativeConnectionProvider _connectionProvider;

		private readonly IAutoConnector _autoConnector;

		private readonly ITransactionsManager _transactionsManager;

		private readonly IEventListenersManager _events;

		private readonly IEntitiesMetadataRegistry _metadataRegistry;

		private readonly bool _prepare;

		private readonly IImmutableList<DbParameter> _parameters;

		public Query(
			INativeConnectionProvider connectionProvider,
			IAutoConnector autoConnector,
			ITransactionsManager transactionsManager,
			IEventListenersManager events,
			IEntitiesMetadataRegistry metadataRegistry,
			bool prepare,
			CommandType commandType,
			string sql,
			IImmutableList<DbParameter> parameters
		)
		{
			_connectionProvider = connectionProvider;
			_autoConnector = autoConnector;
			_transactionsManager = transactionsManager;
			_events = events;
			_metadataRegistry = metadataRegistry;
			_prepare = prepare;
			CommandType = commandType;
			Id = Guid.NewGuid();
			Sql = sql;
			_parameters = parameters;
		}
		
		public CommandType CommandType { get; }
		
		public Guid Id { get; }
		
		public string Sql { get; }

		public IReadOnlyCollection<DbParameter> Parameters => _parameters;

		public IQuery SetSql(string sql)
		{
			return new Query(_connectionProvider, _autoConnector, _transactionsManager, _events, _metadataRegistry, _prepare, CommandType, sql, _parameters);
		}

		public IQuery SetPrepare(bool prepare)
		{
			return new Query(_connectionProvider, _autoConnector, _transactionsManager, _events, _metadataRegistry, prepare, CommandType, Sql, _parameters);
		}

		public IQuery SetParameter(string name, object? value)
		{
			var parameter = new NpgsqlParameter(name, value ?? DBNull.Value);
			return new Query(_connectionProvider, _autoConnector, _transactionsManager, _events, _metadataRegistry, _prepare, CommandType, Sql, _parameters.Add(parameter));
		}

		public IQuery SetParameter<T>(string name, T? value)
		{
			var parameter = value is null ? new NpgsqlParameter(name, DBNull.Value) : new NpgsqlParameter<T>(name, value);
			return new Query(_connectionProvider, _autoConnector, _transactionsManager, _events, _metadataRegistry, _prepare, CommandType, Sql, _parameters.Add(parameter));
		}

		public IAsyncEnumerable<T> GetResult<T>(CancellationToken cancellationToken = default)
			where T : class
		{
			return RunEnumerable<T>((metadata, record, mapping) => metadata.CreateEntity(record, mapping), cancellationToken);
		}

		public async Task<T?> GetSingleOrNullResult<T>(CancellationToken cancellationToken = default)
			where T : class
		{
			var enumerable = RunEnumerable<T>((metadata, record, mapping) => metadata.CreateEntity(record, mapping), cancellationToken);

			T? result = null;
			await foreach (var row in enumerable.WithCancellation(cancellationToken))
			{
				if (result != null)
				{
					throw new NonUniqueResultException(this, "More than one result was found for query, but only one or none was expected");
				}

				result = row;
			}

			return result;
		}

		public async Task<T> GetSingleResult<T>(CancellationToken cancellationToken = default)
			where T : class
		{
			var result = await GetSingleOrNullResult<T>(cancellationToken);
			if (result == null)
			{
				throw new NoResultException(this, "No result was found for query, but exactly one was expected");
			}

			return result;
		}

		public IAsyncEnumerable<T> GetScalarResult<T>(CancellationToken cancellationToken = default)
		{
			return RunEnumerable(record =>
			{
				if (record.FieldCount != 1)
				{
					throw new NonUniqueResultException(this, "More than one column was found for query, but exactly one was expected");
				}

				return record.IsDBNull(0) ? default! : (T)record.GetValue(0);
			}, cancellationToken);
		}

		public Task<T> GetSingleScalarResult<T>(CancellationToken cancellationToken = default)
		{
			return Run(async command =>
			{
				var result = await command.ExecuteScalarAsync(cancellationToken);
				if (result is DBNull)
				{
					return default!;
				}

				return (T) result!;
			}, cancellationToken);
		}

		public Task<int> Execute(CancellationToken cancellationToken = default)
		{
			return Run(command => command.ExecuteNonQueryAsync(cancellationToken), cancellationToken);
		}

		private async Task<T> Run<T>(Func<DbCommand, Task<T>> fn, CancellationToken cancellationToken)
		{
			await using var command = await PrepareCommand(cancellationToken);
			
			Exception? error = null;

			try
			{
				return await fn(command);
			}
			catch (Exception e)
			{
				error = e;
				throw;
			}
			finally
			{
				_events.OnQueryStop(this, error);
			}
		}

		private async IAsyncEnumerable<T> RunEnumerable<T>(Func<IEntityMetadata<T>, IDataRecord, int[], T> fn, [EnumeratorCancellation] CancellationToken cancellationToken)
			where T : class
		{
			IEntityMetadata<T>? metadata = null;
			int[]? mapping = null;

			await foreach (var record in RunEnumerable(record => record, cancellationToken))
			{
				metadata ??= _metadataRegistry.GetMetadata<T>();
				mapping ??= metadata.CreateMapping(record);
				
				yield return fn(metadata, record, mapping);
			}
		}

		private async IAsyncEnumerable<T> RunEnumerable<T>(Func<IDataRecord, T> fn, [EnumeratorCancellation] CancellationToken cancellationToken)
		{
			await using var command = await PrepareCommand(cancellationToken);
			
			DbDataReader? reader = null;
			var eventStopCalled = false;

			try
			{
				reader = await command.ExecuteReaderAsync(cancellationToken);
			}
			catch (Exception e)
			{
				_events.OnQueryStop(this, e);
				throw;
			}

			if (reader.HasRows)
			{
				while (true)
				{
					try
					{
						if (!await reader.ReadAsync(cancellationToken))
						{
							await reader.CloseAsync();
							break;
						}
					}
					catch (Exception e)
					{
						await reader.CloseAsync();
						_events.OnQueryStop(this, e);
						throw;
					}

					if (!eventStopCalled)
					{
						_events.OnQueryStop(this, null);
						eventStopCalled = true;
					}

					yield return fn(reader);
				}
			}

			await reader.CloseAsync();

			if (!eventStopCalled)
			{
				_events.OnQueryStop(this, null);
			}
		}

		private async Task<DbCommand> PrepareCommand(CancellationToken cancellationToken)
		{
			var connection = _connectionProvider.GetConnection() as DbConnection;

			var command = connection.CreateCommand();
			command.CommandType = CommandType;
			command.CommandText = Sql;

			if (_transactionsManager.GetCurrentNativeTransaction() is { } currentNativeTransaction)
			{
				command.Transaction = currentNativeTransaction;
			}

			foreach (var parameter in Parameters)
			{
				command.Parameters.Add(parameter);
			}

			await _autoConnector.Open(connection, cancellationToken);
			_events.OnQueryStart(this);

			if (_prepare)
			{
				try
				{
					await command.PrepareAsync(cancellationToken);
				}
				catch (Exception e)
				{
					_events.OnQueryStop(this, e);
					throw;
				}
			}

			return command;
		}
	}
}
