﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using DKX.Pg.Events;
using DKX.Pg.Metadata;
using DKX.Pg.Queries;
using Npgsql;

namespace DKX.Pg
{
	public interface IConnection : ITransactional, IDisposable, IAsyncDisposable
	{
		NpgsqlConnection NativeConnection { get; }
		
		void AddEventListener(EventListener listener);

		Task TryOpen(CancellationToken cancellationToken = default);
		
		Task Open(CancellationToken cancellationToken = default);

		Task Close();

		IQuery Query(FormattableString sql, CommandType commandType = CommandType.Text);

		IQuery QueryRaw(string sql, CommandType commandType = CommandType.Text);
		
		Task<int> Insert<T>(string table, T data, CancellationToken cancellationToken = default)
			where T : class;

		Task<int> Update<T>(string table, T data, CancellationToken cancellationToken = default)
			where T : class;

		IEntityMetadata<T> LoadEntityMetadata<T>()
			where T : class;

		string Quote(string str);
	}
}
