using System;

namespace DKX.Pg.Exceptions
{
	public sealed class MetadataException : Exception
	{
		internal MetadataException(string message) : base(message)
		{
		}
	}
}
