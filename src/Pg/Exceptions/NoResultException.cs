using System;
using DKX.Pg.Queries;

namespace DKX.Pg.Exceptions
{
	public sealed class NoResultException : Exception
	{
		internal NoResultException(IQuery query, string message) : base(message)
		{
			Query = query;
		}
		
		public IQuery Query { get; }
	}
}
