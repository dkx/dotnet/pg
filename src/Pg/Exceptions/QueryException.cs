﻿using System;

namespace DKX.Pg.Exceptions
{
	public sealed class QueryException : Exception
	{
		internal QueryException(string message) : base(message)
		{
		}
	}
}
