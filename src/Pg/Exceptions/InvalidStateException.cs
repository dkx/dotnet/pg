﻿using System;

namespace DKX.Pg.Exceptions
{
	public sealed class InvalidStateException : Exception
	{
		public InvalidStateException(string message) : base(message)
		{
		}
	}
}
