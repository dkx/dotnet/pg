using System;
using DKX.Pg.Queries;

namespace DKX.Pg.Exceptions
{
	public sealed class NonUniqueResultException : Exception
	{
		internal NonUniqueResultException(IQuery query, string message) : base(message)
		{
			Query = query;
		}
		
		public IQuery Query { get; }
	}
}
