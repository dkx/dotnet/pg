﻿using System;
using DKX.Pg.Queries;

namespace DKX.Pg.Events
{
	public abstract class EventListener
	{
		public virtual void OnQueryStart(IQuery query)
		{
		}
		
		public virtual void OnQueryStop(IQuery query, Exception? exception)
		{
		}
	}
}
