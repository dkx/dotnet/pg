﻿using System;
using DKX.Pg.Queries;

namespace DKX.Pg.Events
{
	internal interface IEventListenersManager
	{
		void Add(EventListener listener);

		void OnQueryStart(IQuery query);

		void OnQueryStop(IQuery query, Exception? exception);
	}
}
