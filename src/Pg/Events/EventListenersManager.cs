﻿using System;
using System.Collections.Generic;
using DKX.Pg.Queries;

namespace DKX.Pg.Events
{
	internal sealed class EventListenersManager : IEventListenersManager
	{
		private readonly IList<EventListener> _listeners = new List<EventListener>();

		public void Add(EventListener listener)
		{
			_listeners.Add(listener);
		}
		
		public void OnQueryStart(IQuery query)
		{
			foreach (var listener in _listeners)
			{
				listener.OnQueryStart(query);
			}
		}
		
		public void OnQueryStop(IQuery query, Exception? exception)
		{
			foreach (var listener in _listeners)
			{
				listener.OnQueryStop(query, exception);
			}
		}
	}
}
