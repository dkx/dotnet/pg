﻿using System.Collections.Generic;
using DKX.Pg.Parsing;

namespace DKX.Pg.Snippets
{
	public static class SetDictionarySnippetConnectionExtensions
	{
		public static SetDictionarySnippet SetDictionary(this IConnection connection, IReadOnlyDictionary<string, object?> data)
		{
			return new SetDictionarySnippet(data);
		}
	}
	
	public sealed class SetDictionarySnippet : ISnippet
	{
		private readonly IReadOnlyDictionary<string, object?> _data;
		
		public SetDictionarySnippet(IReadOnlyDictionary<string, object?> data)
		{
			_data = data;
		}
		
		public string Process(IQueryParsingContext ctx, string? format)
		{
			var pairs = new List<string>();
			foreach (var (column, value) in _data)
			{
				pairs.Add(ctx.Quote(column) + " = " + ctx.AddParameter(value));
			}

			return string.Join(", ", pairs);
		}
	}
}
