using System;
using System.Collections.Generic;
using System.Linq;
using DKX.Pg.Parsing;

namespace DKX.Pg.Snippets
{
	public static class ListSnippetConnectionExtensions
	{
		public static ListSnippet List(this IConnection connection, IReadOnlyCollection<FormattableString> parts)
		{
			return new ListSnippet(parts);
		}

		public static ListSnippet List(this IConnection connection, string delimiter, IReadOnlyCollection<FormattableString> parts)
		{
			return new ListSnippet(parts, delimiter);
		}
	}
	
	public sealed class ListSnippet : ISnippet
	{
		private readonly IReadOnlyCollection<FormattableString> _parts;

		private readonly string _delimiter;

		public ListSnippet(IReadOnlyCollection<FormattableString> parts, string delimiter = ", ")
		{
			_parts = parts;
			_delimiter = delimiter;
		}

		public int Count => _parts.Count;
		
		public string Process(IQueryParsingContext ctx, string? format)
		{
			var parts = _parts.Select(ctx.ParseSqlPart);
			return string.Join(_delimiter, parts);
		}
	}
}
