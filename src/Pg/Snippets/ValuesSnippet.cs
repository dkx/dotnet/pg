using System.Collections.Generic;
using DKX.Pg.Parsing;

namespace DKX.Pg.Snippets
{
	public static class ValuesSnippetConnectionExtensions
	{
		public static ValuesSnippet<T> Values<T>(this IConnection connection, T data)
			where T : class
		{
			return new ValuesSnippet<T>(data);
		}
	}
	
	public sealed class ValuesSnippet<T> : ISnippet
		where T : class
	{
		private readonly T _data;

		public ValuesSnippet(T data)
		{
			_data = data;
		}
		
		public string Process(IQueryParsingContext ctx, string? format)
		{
			var metadata = ctx.LoadEntityMetadata<T>();
			var columns = new List<string>();
			var values = new List<string>();

			foreach (var property in metadata.Properties)
			{
				if (property.IsGenerated)
				{
					continue;
				}

				columns.Add(ctx.Quote(property.DatabaseName));
				values.Add(ctx.AddParameter(property.GetValue(_data)));
			}

			return $"({string.Join(", ", columns)}) VALUES ({string.Join(", ", values)})";
		}
	}
}
