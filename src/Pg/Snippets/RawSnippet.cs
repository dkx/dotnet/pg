using DKX.Pg.Parsing;

namespace DKX.Pg.Snippets
{
	public static class RawSnippetConnectionExtensions
	{
		public static RawSnippet Raw(this IConnection connection, string literal)
		{
			return new RawSnippet(literal);
		}
	}
	
	public sealed class RawSnippet : ISnippet
	{
		private readonly string _literal;

		public RawSnippet(string literal)
		{
			_literal = literal;
		}
		
		public string Process(IQueryParsingContext ctx, string? format)
		{
			return _literal;
		}
	}
}
