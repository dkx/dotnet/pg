﻿using System.Collections.Generic;
using DKX.Pg.Parsing;

namespace DKX.Pg.Snippets
{
	public static class SetSnippetConnectionExtensions
	{
		public static SetSnippet<T> Set<T>(this IConnection connection, T data)
			where T : class
		{
			return new SetSnippet<T>(data);
		}
	}
	
	public sealed class SetSnippet<T> : ISnippet
		where T : class
	{
		private readonly T _data;
		
		public SetSnippet(T data)
		{
			_data = data;
		}
		
		public string Process(IQueryParsingContext ctx, string? format)
		{
			var metadata = ctx.LoadEntityMetadata<T>();
			var pairs = new List<string>();

			foreach (var property in metadata.Properties)
			{
				if (property.IsIdentifier)
				{
					continue;
				}
				
				pairs.Add(ctx.Quote(property.DatabaseName) + " = " + ctx.AddParameter(property.GetValue(_data)));
			}

			return string.Join(", ", pairs);
		}
	}
}
