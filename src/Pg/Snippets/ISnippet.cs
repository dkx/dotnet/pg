using DKX.Pg.Parsing;

namespace DKX.Pg.Snippets
{
	public interface ISnippet
	{
		string Process(IQueryParsingContext ctx, string? format);
	}
}
