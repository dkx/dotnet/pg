using DKX.Pg.Parsing;

namespace DKX.Pg.Snippets
{
	public static class ParameterTypedSnippetConnectionExtensions
	{
		public static ParameterTypedSnippet<T> Param<T>(this IConnection connection, T value)
		{
			return new ParameterTypedSnippet<T>(value);
		}
	}

	public sealed class ParameterTypedSnippet<T> : ISnippet
	{
		private readonly T _value;
		
		public ParameterTypedSnippet(T value)
		{
			_value = value;
		}
		
		public string Process(IQueryParsingContext ctx, string? format)
		{
			return ctx.AddParameter(_value);
		}
	}
}
