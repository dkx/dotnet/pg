using System;
using DKX.Pg.Parsing;

namespace DKX.Pg.Snippets
{
	public static class IfSnippetConnectionExtensions
	{
		public static IfSnippet If(this IConnection connection, bool condition, FormattableString thenSql, FormattableString? elseSql = null)
		{
			return new IfSnippet(condition, thenSql, elseSql);
		}
	}
	
	public sealed class IfSnippet : ISnippet
	{
		private readonly bool _condition;

		private readonly FormattableString _then;

		private readonly FormattableString? _else;

		public IfSnippet(bool condition, FormattableString thenSql, FormattableString? elseSql = null)
		{
			_condition = condition;
			_then = thenSql;
			_else = elseSql;
		}
		
		public string Process(IQueryParsingContext ctx, string? format)
		{
			if (!_condition)
			{
				if (_else == null)
				{
					return "";
				}

				return ctx.ParseSqlPart(_else);
			}

			return ctx.ParseSqlPart(_then);
		}
	}
}
