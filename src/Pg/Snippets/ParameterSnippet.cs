using DKX.Pg.Parsing;

namespace DKX.Pg.Snippets
{
	public static class ParameterSnippetConnectionExtensions
	{
		public static ParameterSnippet Param(this IConnection connection, object value)
		{
			return new ParameterSnippet(value);
		}
	}
	
	public sealed class ParameterSnippet : ISnippet
	{
		private readonly object? _value;

		public ParameterSnippet(object? value)
		{
			_value = value;
		}

		public string Process(IQueryParsingContext ctx, string? format)
		{
			return ctx.AddParameter(_value);
		}
	}
}
