using System;
using System.Collections.Generic;
using System.Linq;
using DKX.Pg.Parsing;

namespace DKX.Pg.Snippets
{
	public static class CompositeSnippetConnectionExtensions
	{
		public static CompositeSnippet And(this IConnection connection, IReadOnlyCollection<FormattableString> parts)
		{
			return new CompositeSnippet(CompositeSnippetType.And, parts);
		}
		
		public static CompositeSnippet And(this IConnection connection, params FormattableString[] parts)
		{
			return new CompositeSnippet(CompositeSnippetType.And, parts);
		}

		public static CompositeSnippet Or(this IConnection connection, IReadOnlyCollection<FormattableString> parts)
		{
			return new CompositeSnippet(CompositeSnippetType.Or, parts);
		}
		
		public static CompositeSnippet Or(this IConnection connection, params FormattableString[] parts)
		{
			return new CompositeSnippet(CompositeSnippetType.Or, parts);
		}
	}
	
	public sealed class CompositeSnippet : ISnippet
	{
		private readonly CompositeSnippetType _type;

		private readonly IReadOnlyCollection<FormattableString> _parts;

		public CompositeSnippet(CompositeSnippetType type, IReadOnlyCollection<FormattableString> parts)
		{
			_type = type;
			_parts = parts;
		}

		public int Count => _parts.Count;

		public string Process(IQueryParsingContext ctx, string? format)
		{
			var parts = _parts.Select(part => $"({ctx.ParseSqlPart(part)})");
			return string.Join($" {_type.ToString().ToUpper()} ", parts);
		}
	}

	public enum CompositeSnippetType
	{
		And,
		Or,
	}
}
