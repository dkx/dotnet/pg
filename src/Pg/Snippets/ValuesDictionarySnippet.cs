﻿using System.Collections.Generic;
using DKX.Pg.Parsing;

namespace DKX.Pg.Snippets
{
	public static class ValuesDictionarySnippetConnectionExtensions
	{
		public static ValuesDictionarySnippet ValuesDictionary(this IConnection connection, IReadOnlyDictionary<string, object?> data)
		{
			return new ValuesDictionarySnippet(data);
		}
	}
	
	public sealed class ValuesDictionarySnippet : ISnippet
	{
		private readonly IReadOnlyDictionary<string, object?> _data;
		
		public ValuesDictionarySnippet(IReadOnlyDictionary<string, object?> data)
		{
			_data = data;
		}
		
		public string Process(IQueryParsingContext ctx, string? format)
		{
			var columns = new List<string>();
			var values = new List<string>();

			foreach (var (column, value) in _data)
			{
				columns.Add(ctx.Quote(column));
				values.Add(ctx.AddParameter(value));
			}
			
			return $"({string.Join(", ", columns)}) VALUES ({string.Join(", ", values)})";
		}
	}
}
