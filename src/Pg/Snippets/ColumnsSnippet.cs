using System.Linq;
using DKX.Pg.Parsing;

namespace DKX.Pg.Snippets
{
	public static class ColumnsSnippetConnectionExtensions
	{
		public static ColumnsSnippet<T> Columns<T>(this IConnection connection, string? alias = null)
			where T : class
		{
			return new ColumnsSnippet<T>(alias);
		}
	}
	
	public sealed class ColumnsSnippet<T> : ISnippet
		where T : class
	{
		private readonly string? _alias;

		public ColumnsSnippet(string? alias = null)
		{
			_alias = alias;
		}
		
		public string Process(IQueryParsingContext ctx, string? format)
		{
			var columns = ctx.LoadEntityMetadata<T>().Properties.Select(p => ctx.Quote(p.DatabaseName));

			return string.Join(
				", ",
				_alias == null ? columns : columns.Select(c => _alias + "." + c)
			);
		}
	}
}
