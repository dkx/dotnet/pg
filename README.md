# DKX.Pg

PostgreSQL only client with something better than query builder or LINQ (hint: pure SQL is better)

* PostgreSQL only
* Async only
* Immutable entities
* Opening connection automatically when needed
* Automatic nested [transactions](.docs/transactions.md) (with savepoints)
* Plain SQL with [snippets](.docs/snippets.md)
* .net 5 only

## Installation

Install main package:

```bash
$ dotnet add package DKX.Pg
$ dotnet add package DKX.Pg.AOT
```

## Example

```c#
using DKX.Pg;
using DKX.Pg.Entities;
using DKX.Pg.Metadata;

[Entity]
public partial class User
{
    [Id, Generated]
    public int Id { get; }
    
    public string Email { get; }
}

await using var db = new Connection(new NpgsqlConnection(connectionString), new AssemblyEntitiesMetadataProvider(assemblyWithEntities));

var email = "john@doe.com";
var query = db.Query($"SELECT id, email FROM users WHERE email = {email}"); // <- No worry, that is not "SQL injection ready" query, read more ;-)

// SQL: SELECT id, email FROM users WHERE email = $1

var user = await query.GetSingleResult<User>();
```

## Documentation

* [Configuration](.docs/configuration.md)
* [Mapping](.docs/mapping.md)
* [Query](.docs/query.md)
* [Snippets](.docs/snippets.md)
* [Transactions](.docs/transactions.md)
* [Logging](.docs/logging.md)
