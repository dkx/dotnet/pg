﻿using System;
using System.Linq;
using DKX.Pg.Metadata;
using DKX.Pg.Parsing;
using Npgsql;
using Xunit;

namespace DKX.Pg.Tests.Parsing
{
	public sealed class SqlParserWithLegacyNamedParametersTest
	{
		private readonly SqlParser _parser;

		private readonly QueryParsingContext _ctx;

		public SqlParserWithLegacyNamedParametersTest()
		{
			_parser = new SqlParser();
			_ctx = new QueryParsingContext(
				_parser,
				new EntitiesMetadataRegistry(new AssemblyEntitiesMetadataProvider(GetType().Assembly)),
				true
			);
		}
		
		[Fact]
		public void Parse_Simple()
		{
			var parsed = _parser.Parse($"SELECT * FROM users", _ctx);
			Assert.Equal("SELECT * FROM users", parsed);
			Assert.Empty(_ctx.Parameters);
		}
		
		[Fact]
		public void Parse_ImplicitParameter()
		{
			var parsed = _parser.Parse($"SELECT * FROM users WHERE email = {"john@doe"}", _ctx);
			var parameters = _ctx.Parameters.ToArray();
			
			Assert.Equal("SELECT * FROM users WHERE email = @p0", parsed);
			Assert.Single(parameters);
			Assert.IsType<NpgsqlParameter>(parameters[0]);
			Assert.Equal("p0", parameters[0].ParameterName);
			Assert.Equal("john@doe", parameters[0].Value);
		}
		
		[Fact]
		public void Parse_InnerTemplateWithImplicitParameter()
		{
			FormattableString where = $"WHERE email = {"john@doe"}";
			var parsed = _parser.Parse($"SELECT * FROM users {where}", _ctx);
			var parameters = _ctx.Parameters.ToArray();
			
			Assert.Equal("SELECT * FROM users WHERE email = @p0", parsed);
			Assert.Single(parameters);
			Assert.IsType<NpgsqlParameter>(parameters[0]);
			Assert.Equal("p0", parameters[0].ParameterName);
			Assert.Equal("john@doe", parameters[0].Value);
		}
	}
}
