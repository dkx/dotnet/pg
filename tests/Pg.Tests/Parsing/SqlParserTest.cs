﻿using System;
using System.Linq;
using DKX.Pg.Metadata;
using DKX.Pg.Parsing;
using Npgsql;
using Xunit;

namespace DKX.Pg.Tests.Parsing
{
	public sealed class SqlParserTest
	{
		private readonly SqlParser _parser;

		private readonly QueryParsingContext _ctx;

		public SqlParserTest()
		{
			_parser = new SqlParser();
			_ctx = new QueryParsingContext(
				_parser,
				new EntitiesMetadataRegistry(new AssemblyEntitiesMetadataProvider(GetType().Assembly)),
				false
			);
		}
		
		[Fact]
		public void Parse_Simple()
		{
			var parsed = _parser.Parse($"SELECT * FROM users", _ctx);
			Assert.Equal("SELECT * FROM users", parsed);
			Assert.Empty(_ctx.Parameters);
		}
		
		[Fact]
		public void Parse_ImplicitParameter()
		{
			var parsed = _parser.Parse($"SELECT * FROM users WHERE email = {"john@doe"}", _ctx);
			var parameters = _ctx.Parameters.ToArray();
			
			Assert.Equal("SELECT * FROM users WHERE email = $1", parsed);
			Assert.Single(parameters);
			Assert.IsType<NpgsqlParameter>(parameters[0]);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal("john@doe", parameters[0].Value);
		}
		
		[Fact]
		public void Parse_InnerTemplateWithImplicitParameter()
		{
			FormattableString where = $"WHERE email = {"john@doe"}";
			var parsed = _parser.Parse($"SELECT * FROM users {where}", _ctx);
			var parameters = _ctx.Parameters.ToArray();
			
			Assert.Equal("SELECT * FROM users WHERE email = $1", parsed);
			Assert.Single(parameters);
			Assert.IsType<NpgsqlParameter>(parameters[0]);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal("john@doe", parameters[0].Value);
		}
	}
}
