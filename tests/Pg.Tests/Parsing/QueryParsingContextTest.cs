﻿using System.Linq;
using DKX.Pg.Metadata;
using DKX.Pg.Parsing;
using DKX.Pg.Tests.Data;
using Xunit;

namespace DKX.Pg.Tests.Parsing
{
	public sealed class QueryParsingContextTest
	{
		private readonly QueryParsingContext _ctx;

		public QueryParsingContextTest()
		{
			_ctx = new QueryParsingContext(
				new SqlParser(),
				new EntitiesMetadataRegistry(new AssemblyEntitiesMetadataProvider(GetType().Assembly)),
				false
			);
		}

		[Fact]
		public void AddParameter()
		{
			_ctx.AddParameter("first");
			_ctx.AddParameter("second");
			_ctx.AddParameter("third");
			
			var parameters = _ctx.Parameters.ToArray();

			Assert.Equal(3, parameters.Length);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal("first", parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("second", parameters[1].Value);
			Assert.Equal("", parameters[2].ParameterName);
			Assert.Equal("third", parameters[2].Value);
		}

		[Fact]
		public void ParseSqlPart()
		{
			var parsed = _ctx.ParseSqlPart($"SELECT * FROM users WHERE email = {"john@doe"}");
			
			Assert.Equal("SELECT * FROM users WHERE email = $1", parsed);
			Assert.Equal(1, _ctx.Parameters.Count);
		}

		[Fact]
		public void LoadEntityMetadata()
		{
			var metadata = _ctx.LoadEntityMetadata<Book>();
			Assert.IsType<BookEntityMetadata>(metadata);
		}
	}
}
