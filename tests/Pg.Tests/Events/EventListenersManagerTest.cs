﻿using System;
using System.Collections.Generic;
using DKX.Pg.Events;
using DKX.Pg.Queries;
using DKX.Pg.Tests.Data;
using Moq;
using Xunit;

namespace DKX.Pg.Tests.Events
{
	public sealed class EventListenersManagerTest
	{
		private readonly EventListenersManager _events = new EventListenersManager();

		private readonly IList<string> _logs = new List<string>();

		private readonly IQuery _query = new Mock<IQuery>().Object;

		public EventListenersManagerTest()
		{
			_events.Add(new LoggingEventListener("a", _logs));
			_events.Add(new LoggingEventListener("b", _logs));
			_events.Add(new LoggingEventListener("c", _logs));
		}

		[Fact]
		public void OnQueryStart()
		{
			Assert.Empty(_logs);
			
			_events.OnQueryStart(_query);

			Assert.Equal(new[]
			{
				"a: OnQueryStart",
				"b: OnQueryStart",
				"c: OnQueryStart",
			}, _logs);
		}

		[Fact]
		public void OnQueryStop()
		{
			Assert.Empty(_logs);
			
			_events.OnQueryStop(_query, null);

			Assert.Equal(new[]
			{
				"a: OnQueryStop",
				"b: OnQueryStop",
				"c: OnQueryStop",
			}, _logs);
		}

		[Fact]
		public void OnQueryStop_WithException()
		{
			Assert.Empty(_logs);
			
			_events.OnQueryStop(_query, new Exception("Test"));

			Assert.Equal(new[]
			{
				"a: OnQueryStop (exception: Test)",
				"b: OnQueryStop (exception: Test)",
				"c: OnQueryStop (exception: Test)",
			}, _logs);
		}
	}
}
