﻿using System;
using System.Data;
using DKX.Pg.Exceptions;
using DKX.Pg.Metadata;
using DKX.Pg.Tests.Data;
using Xunit;

namespace DKX.Pg.Tests.Metadata
{
	public sealed class EntityMappingsRegistryTest
	{
		private readonly EntitiesMetadataRegistry _registry = new EntitiesMetadataRegistry(new StaticEntitiesMetadataProvider(new[]
		{
			typeof(TestMapping),
		}));
		
		[Fact]
		public void GetMapping()
		{
			Assert.IsType<TestMapping>(_registry.GetMetadata<TestEntity>());
		}

		[Fact]
		public void GetMapping_ThrowMissingEntityAttribute()
		{
			var e = Assert.Throws<MetadataException>(() => _registry.GetMetadata<InvalidEntity>());
			Assert.Equal("Entity 'DKX.Pg.Tests.Metadata.InvalidEntity' is missing [Entity] class attribute", e.Message);
		}

		private sealed class TestEntity
		{
		}

		private sealed class TestMapping : IEntityMetadata<TestEntity>
		{
			public IPropertyMetadata<TestEntity>[] Properties { get; } = Array.Empty<IPropertyMetadata<TestEntity>>();

			public int[] CreateMapping(IDataRecord record)
			{
				throw new System.NotImplementedException();
			}

			public TestEntity CreateEntity(IDataRecord record, int[] mapping)
			{
				throw new System.NotImplementedException();
			}
		}
	}

	public class InvalidEntity
	{
	}
}
