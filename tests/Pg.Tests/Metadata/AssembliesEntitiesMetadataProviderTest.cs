﻿using System.Linq;
using DKX.Pg.Metadata;
using DKX.Pg.Tests.Data;
using Xunit;

namespace DKX.Pg.Tests.Metadata
{
	public sealed class AssembliesEntitiesMetadataProviderTest
	{
		private readonly AssemblyEntitiesMetadataProvider _finder;

		public AssembliesEntitiesMetadataProviderTest()
		{
			_finder = new AssemblyEntitiesMetadataProvider(GetType().Assembly);
		}
		
		[Fact]
		public void GetEntitiesMetadataTypes()
		{
			var types = _finder.GetEntitiesMetadataTypes().ToArray();

			Assert.Equal(5, types.Length);
			
			// works after compiling the project
			Assert.Contains(typeof(BookEntityMetadata), types);
			Assert.Contains(typeof(LocationEntityMetadata), types);
			Assert.Contains(typeof(NoteEntityMetadata), types);
			Assert.Contains(typeof(UserEntityMetadata), types);
		}
	}
}
