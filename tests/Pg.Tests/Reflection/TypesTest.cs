﻿using System;
using System.Linq;
using DKX.Pg.Reflection;
using Xunit;

namespace DKX.Pg.Tests.Reflection
{
	public sealed class TypesTest
	{
		[Theory]
		[InlineData("BoolNotNull", typeof(bool), false)]
		[InlineData("BoolNullable", typeof(bool), true)]
		[InlineData("ByteNotNull", typeof(byte), false)]
		[InlineData("ByteNullable", typeof(byte), true)]
		[InlineData("CharNotNull", typeof(char), false)]
		[InlineData("CharNullable", typeof(char), true)]
		[InlineData("DateTimeNotNull", typeof(DateTime), false)]
		[InlineData("DateTimeNullable", typeof(DateTime), true)]
		[InlineData("DecimalNotNull", typeof(decimal), false)]
		[InlineData("DecimalNullable", typeof(decimal), true)]
		[InlineData("DoubleNotNull", typeof(double), false)]
		[InlineData("DoubleNullable", typeof(double), true)]
		[InlineData("FloatNotNull", typeof(float), false)]
		[InlineData("FloatNullable", typeof(float), true)]
		[InlineData("GuidNotNull", typeof(Guid), false)]
		[InlineData("GuidNullable", typeof(Guid), true)]
		[InlineData("ShortNotNull", typeof(short), false)]
		[InlineData("ShortNullable", typeof(short), true)]
		[InlineData("IntNotNull", typeof(int), false)]
		[InlineData("IntNullable", typeof(int), true)]
		[InlineData("LongNotNull", typeof(long), false)]
		[InlineData("LongNullable", typeof(long), true)]
		[InlineData("StringNotNull", typeof(string), true)]		// always true
		[InlineData("StringNullable", typeof(string), true)]
		[InlineData("ObjectNotNull", typeof(object), false)]
		[InlineData("ObjectNullable", typeof(object), true)]
		[InlineData("ClassNotNull", typeof(Composite), false)]
		[InlineData("ClassNullable", typeof(Composite), true)]
		[InlineData("InterfaceNotNull", typeof(IComposite), false)]
		[InlineData("InterfaceNullable", typeof(IComposite), true)]
		[InlineData("EnumNotNull", typeof(SomeEnum), false)]
		[InlineData("EnumNullable", typeof(SomeEnum), true)]
		public void GetRealType_Property(string property, Type realType, bool isNullable)
		{
			var result = Types.GetRealType(typeof(EntityWithProperties).GetProperty(property)!);

			Assert.Equal(realType, result.RealType);
			Assert.Equal(isNullable, result.IsNullable);
		}
		
		[Theory]
		[InlineData("boolNotNull", typeof(bool), false)]
		[InlineData("boolNullable", typeof(bool), true)]
		[InlineData("byteNotNull", typeof(byte), false)]
		[InlineData("byteNullable", typeof(byte), true)]
		[InlineData("charNotNull", typeof(char), false)]
		[InlineData("charNullable", typeof(char), true)]
		[InlineData("dateTimeNotNull", typeof(DateTime), false)]
		[InlineData("dateTimeNullable", typeof(DateTime), true)]
		[InlineData("decimalNotNull", typeof(decimal), false)]
		[InlineData("decimalNullable", typeof(decimal), true)]
		[InlineData("doubleNotNull", typeof(double), false)]
		[InlineData("doubleNullable", typeof(double), true)]
		[InlineData("floatNotNull", typeof(float), false)]
		[InlineData("floatNullable", typeof(float), true)]
		[InlineData("guidNotNull", typeof(Guid), false)]
		[InlineData("guidNullable", typeof(Guid), true)]
		[InlineData("shortNotNull", typeof(short), false)]
		[InlineData("shortNullable", typeof(short), true)]
		[InlineData("intNotNull", typeof(int), false)]
		[InlineData("intNullable", typeof(int), true)]
		[InlineData("longNotNull", typeof(long), false)]
		[InlineData("longNullable", typeof(long), true)]
		[InlineData("stringNotNull", typeof(string), true)]		// always true
		[InlineData("stringNullable", typeof(string), true)]
		[InlineData("objectNotNull", typeof(object), false)]
		[InlineData("objectNullable", typeof(object), true)]
		[InlineData("classNotNull", typeof(Composite), false)]
		[InlineData("classNullable", typeof(Composite), true)]
		[InlineData("interfaceNotNull", typeof(IComposite), false)]
		[InlineData("interfaceNullable", typeof(IComposite), true)]
		[InlineData("enumNotNull", typeof(SomeEnum), false)]
		[InlineData("enumNullable", typeof(SomeEnum), true)]
		public void GetRealType_Constructor(string parameter, Type realType, bool isNullable)
		{
			var parameters = typeof(EntityWithConstructor).GetConstructors()[0].GetParameters();
			var result = Types.GetRealType(parameters.First(p => p.Name == parameter));

			Assert.Equal(realType, result.RealType);
			Assert.Equal(isNullable, result.IsNullable);
		}

		private class EntityWithProperties
		{
			public bool BoolNotNull { get; }
			public bool? BoolNullable { get; }
			public byte ByteNotNull { get; }
			public byte? ByteNullable { get; }
			public char CharNotNull { get; }
			public char? CharNullable { get; }
			public DateTime DateTimeNotNull { get; }
			public DateTime? DateTimeNullable { get; }
			public decimal DecimalNotNull { get; }
			public decimal? DecimalNullable { get; }
			public double DoubleNotNull { get; }
			public double? DoubleNullable { get; }
			public float FloatNotNull { get; }
			public float? FloatNullable { get; }
			public Guid GuidNotNull { get; }
			public Guid? GuidNullable { get; }
			public short ShortNotNull { get; }
			public short? ShortNullable { get; }
			public int IntNotNull { get; }
			public int? IntNullable { get; }
			public long LongNotNull { get; }
			public long? LongNullable { get; }
			public string StringNotNull { get; }
			public string? StringNullable { get; }
			public object ObjectNotNull { get; }
			public object? ObjectNullable { get; }
			public Composite ClassNotNull { get; }
			public Composite? ClassNullable { get; }
			public IComposite InterfaceNotNull { get; }
			public IComposite? InterfaceNullable { get; }
			public SomeEnum EnumNotNull { get; }
			public SomeEnum? EnumNullable { get; }
		}

		private class EntityWithConstructor
		{
			public EntityWithConstructor(
				bool boolNotNull,
				bool? boolNullable,
				byte byteNotNull,
				byte? byteNullable,
				char charNotNull,
				char? charNullable,
				DateTime dateTimeNotNull,
				DateTime? dateTimeNullable,
				decimal decimalNotNull,
				decimal? decimalNullable,
				double doubleNotNull,
				double? doubleNullable,
				float floatNotNull,
				float? floatNullable,
				Guid guidNotNull,
				Guid? guidNullable,
				short shortNotNull,
				short? shortNullable,
				int intNotNull,
				int? intNullable,
				long longNotNull,
				long? longNullable,
				string stringNotNull,
				string? stringNullable,
				object objectNotNull,
				object? objectNullable,
				Composite classNotNull,
				Composite? classNullable,
				IComposite interfaceNotNull,
				IComposite? interfaceNullable,
				SomeEnum enumNotNull,
				SomeEnum? enumNullable
			) {}
		}

		private interface IComposite {}

		private class Composite : IComposite {}
		
		private enum SomeEnum {}
	}
}
