﻿using DKX.Pg.Snippets;
using DKX.Pg.Tests.Utils;
using Xunit;

namespace DKX.Pg.Tests.Snippets
{
	public sealed class RawSnippetConnectionExtensionsTest
	{
		[Fact]
		public void Raw()
		{
			var part = SnippetRunner.Run(connection => connection.Raw("SELECT 1"), out var ctx);
			Assert.Equal("SELECT 1", part);
		}
	}
	
	public sealed class RawSnippetTest
	{
		[Fact]
		public void Parse()
		{
			var part = SnippetRunner.Run(new RawSnippet("SELECT 1"), out var ctx);

			Assert.Equal("SELECT 1", part);
			Assert.Equal(0, ctx.Parameters.Count);
		}
	}
}
