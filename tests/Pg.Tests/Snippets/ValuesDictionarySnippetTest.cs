﻿using System;
using System.Collections.Generic;
using System.Linq;
using DKX.Pg.Snippets;
using DKX.Pg.Tests.Utils;
using Xunit;

namespace DKX.Pg.Tests.Snippets
{
	public sealed class ValuesDictionarySnippetConnectionExtensionsTest
	{
		[Fact]
		public void ValuesDictionary()
		{
			var part = SnippetRunner.Run(connection => connection.ValuesDictionary(new Dictionary<string, object?>
			{
				["id"] = 5,
				["name"] = "Harry Potter",
				["access_key"] = "abcd",
			}), out var ctx);

			Assert.Equal("(\"id\", \"name\", \"access_key\") VALUES ($1, $2, $3)", part);
		}
	}
	
	public sealed class ValuesDictionarySnippetTest
	{
		[Fact]
		public void Parse()
		{
			var part = SnippetRunner.Run(new ValuesDictionarySnippet(new Dictionary<string, object?>
			{
				["id"] = 5,
				["name"] = "Harry Potter",
				["access_key"] = "abcd",
			}), out var ctx);
			var parameters = ctx.Parameters.ToArray();

			Assert.Equal("(\"id\", \"name\", \"access_key\") VALUES ($1, $2, $3)", part);
			Assert.Equal(3, parameters.Length);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal(5, parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("Harry Potter", parameters[1].Value);
			Assert.Equal("", parameters[2].ParameterName);
			Assert.Equal("abcd", parameters[2].Value);
		}
		
		[Fact]
		public void Parse_WithNull()
		{
			var part = SnippetRunner.Run(new ValuesDictionarySnippet(new Dictionary<string, object?>
			{
				["id"] = 5,
				["name"] = "Harry Potter",
				["access_key"] = null,
			}), out var ctx);
			var parameters = ctx.Parameters.ToArray();

			Assert.Equal("(\"id\", \"name\", \"access_key\") VALUES ($1, $2, $3)", part);
			Assert.Equal(3, parameters.Length);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal(5, parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("Harry Potter", parameters[1].Value);
			Assert.Equal("", parameters[2].ParameterName);
			Assert.Same(DBNull.Value, parameters[2].Value);
		}
	}
}
