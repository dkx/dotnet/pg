﻿using System;
using System.Linq;
using DKX.Pg.Snippets;
using DKX.Pg.Tests.Utils;
using Xunit;

namespace DKX.Pg.Tests.Snippets
{
	public sealed class CompositeSnippetConnectionExtensionsTest
	{
		[Fact]
		public void And()
		{
			var part = SnippetRunner.Run(connection => connection.And(new FormattableString[]
			{
				$"name ILIKE {"Harry %"}",
				$"name ILIKE {"% Potter"}",
			}), out var ctx);
			
			Assert.Equal("(name ILIKE $1) AND (name ILIKE $2)", part);
		}
		
		[Fact]
		public void Or()
		{
			var part = SnippetRunner.Run(connection => connection.Or(new FormattableString[]
			{
				$"name ILIKE {"Harry %"}",
				$"name ILIKE {"Lord %"}",
			}), out var ctx);
			
			Assert.Equal("(name ILIKE $1) OR (name ILIKE $2)", part);
		}
	}
	
	public sealed class CompositeSnippetTest
	{
		[Fact]
		public void Parse_And()
		{
			var part = SnippetRunner.Run(new CompositeSnippet(
				CompositeSnippetType.And,
				new FormattableString[]
				{
					$"name ILIKE {"Harry %"}",
					$"name ILIKE {"% Potter"}",
				}
			), out var ctx);
			var parameters = ctx.Parameters.ToArray();

			Assert.Equal("(name ILIKE $1) AND (name ILIKE $2)", part);
			Assert.Equal(2, ctx.Parameters.Count);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal("Harry %", parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("% Potter", parameters[1].Value);
		}
		
		[Fact]
		public void Parse_Or()
		{
			var part = SnippetRunner.Run(new CompositeSnippet(
				CompositeSnippetType.Or,
				new FormattableString[]
				{
					$"name ILIKE {"Harry %"}",
					$"name ILIKE {"Lord %"}",
				}
			), out var ctx);
			var parameters = ctx.Parameters.ToArray();

			Assert.Equal("(name ILIKE $1) OR (name ILIKE $2)", part);
			Assert.Equal(2, ctx.Parameters.Count);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal("Harry %", parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("Lord %", parameters[1].Value);
		}
		
		[Fact]
		public void Parse_Nested()
		{
			var part = SnippetRunner.Run(new CompositeSnippet(
				CompositeSnippetType.Or,
				new FormattableString[]
				{
					$"name ILIKE {"Harry %"}",
					$"name ILIKE {"Lord %"}",
					$@"{new CompositeSnippet(CompositeSnippetType.And, new FormattableString[]
					{
						$"external_id IS NOT NULL",
						$"id = 2",
						$"name != {"Hitchhiker's Guide to the Galaxy"}",
					})}",
				}
			), out var ctx);
			var parameters = ctx.Parameters.ToArray();

			Assert.Equal("(name ILIKE $1) OR (name ILIKE $2) OR ((external_id IS NOT NULL) AND (id = 2) AND (name != $3))", part);
			Assert.Equal(3, ctx.Parameters.Count);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal("Harry %", parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("Lord %", parameters[1].Value);
			Assert.Equal("", parameters[2].ParameterName);
			Assert.Equal("Hitchhiker's Guide to the Galaxy", parameters[2].Value);
		}
	}
}
