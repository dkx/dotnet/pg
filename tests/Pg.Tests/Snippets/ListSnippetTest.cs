﻿using System;
using DKX.Pg.Snippets;
using DKX.Pg.Tests.Utils;
using Xunit;

namespace DKX.Pg.Tests.Snippets
{
	public sealed class ListSnippetConnectionExtensionsTest
	{
		[Fact]
		public void List()
		{
			var part = SnippetRunner.Run(connection => connection.List(new FormattableString[]
			{
				$"name DESC",
				$"id ASC",
			}), out var ctx);
			
			Assert.Equal("name DESC, id ASC", part);
		}
		
		[Fact]
		public void List_CustomDelimiter()
		{
			var part = SnippetRunner.Run(connection => connection.List(" ", new FormattableString[]
			{
				$"a",
				$"b",
			}), out var ctx);
			
			Assert.Equal("a b", part);
		}
	}
	
	public sealed class ListSnippetTest
	{
		[Fact]
		public void Parse()
		{
			var part = SnippetRunner.Run(new ListSnippet(new FormattableString[]
			{
				$"name DESC",
				$"id ASC",
			}), out var ctx);

			Assert.Equal("name DESC, id ASC", part);
			Assert.Equal(0, ctx.Parameters.Count);
		}
		[Fact]
		public void Parse_CustomDelimiter()
		{
			var part = SnippetRunner.Run(new ListSnippet(new FormattableString[]
			{
				$"a",
				$"b",
			}, " "), out var ctx);

			Assert.Equal("a b", part);
			Assert.Equal(0, ctx.Parameters.Count);
		}
	}
}
