﻿using System.Linq;
using DKX.Pg.Snippets;
using DKX.Pg.Tests.Utils;
using Xunit;

namespace DKX.Pg.Tests.Snippets
{
	public sealed class ParameterTypedSnippetConnectionExtensionsTest
	{
		[Fact]
		public void Param()
		{
			var part = SnippetRunner.Run(connection => connection.Param<string>("% Potter"), out var ctx);
			Assert.Equal("$1", part);
		}
	}
	
	public sealed class ParameterTypedSnippetTest
	{
		[Fact]
		public void Parse_Typed()
		{
			var part = SnippetRunner.Run(new ParameterTypedSnippet<string>("% Potter"), out var ctx);

			Assert.Equal("$1", part);
			Assert.Equal(1, ctx.Parameters.Count);
			Assert.Equal("", ctx.Parameters.First().ParameterName);
			Assert.Equal("% Potter", ctx.Parameters.First().Value);
		}
	}
}
