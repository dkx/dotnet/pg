﻿using System;
using System.Linq;
using DKX.Pg.Snippets;
using DKX.Pg.Tests.Data;
using DKX.Pg.Tests.Utils;
using Xunit;

namespace DKX.Pg.Tests.Snippets
{
	public sealed class ValuesSnippetConnectionExtensionsTest
	{
		[Fact]
		public void Values()
		{
			var book = new Note(Guid.NewGuid(), "lorem ipsum") { State = true };
			var part = SnippetRunner.Run(connection => connection.Values(book), out var ctx);
			
			Assert.Equal("(\"id\", \"content\", \"state\") VALUES ($1, $2, $3)", part);
		}
	}
	
	public sealed class ValuesSnippetTest
	{
		[Fact]
		public void Parse()
		{
			var id = Guid.NewGuid();
			var note = new Note(id, "lorem ipsum") { State = true };
			var part = SnippetRunner.Run(new ValuesSnippet<Note>(note), out var ctx);
			var parameters = ctx.Parameters.ToArray();

			Assert.Equal("(\"id\", \"content\", \"state\") VALUES ($1, $2, $3)", part);
			Assert.Equal(3, parameters.Length);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal(id, parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("lorem ipsum", parameters[1].Value);
			Assert.Equal("", parameters[2].ParameterName);
			Assert.Equal(true, parameters[2].Value);
		}
		
		[Fact]
		public void Parse_WithNull()
		{
			var id = Guid.NewGuid();
			var note = new Note(id, "lorem ipsum");
			var part = SnippetRunner.Run(new ValuesSnippet<Note>(note), out var ctx);
			var parameters = ctx.Parameters.ToArray();

			Assert.Equal("(\"id\", \"content\", \"state\") VALUES ($1, $2, $3)", part);
			Assert.Equal(3, parameters.Length);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal(id, parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("lorem ipsum", parameters[1].Value);
			Assert.Equal("", parameters[2].ParameterName);
			Assert.Equal(DBNull.Value, parameters[2].Value);
		}
		
		[Fact]
		public void Parse_WithoutIds()
		{
			var part = SnippetRunner.Run(
				new ValuesSnippet<Book>(new Book(5, BookState.Finished, "Harry Potter") {AccessKey = "abcd"}),
				out var ctx
			);
			
			var parameters = ctx.Parameters.ToArray();

			Assert.Equal("(\"state\", \"name\", \"access_key\") VALUES ($1, $2, $3)", part);
			Assert.Equal(3, parameters.Length);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal(BookState.Finished, parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("Harry Potter", parameters[1].Value);
			Assert.Equal("", parameters[2].ParameterName);
			Assert.Equal("abcd", parameters[2].Value);
		}
	}
}
