﻿using System;
using System.Linq;
using DKX.Pg.Snippets;
using DKX.Pg.Tests.Data;
using DKX.Pg.Tests.Utils;
using Xunit;

namespace DKX.Pg.Tests.Snippets
{
	public sealed class SetSnippetConnectionExtensionsTest
	{
		[Fact]
		public void Set()
		{
			var book = new Book(5, BookState.InProgress, "Harry Potter") { AccessKey = "abcd" };
			var part = SnippetRunner.Run(connection => connection.Set(book), out var ctx);
			
			Assert.Equal("\"state\" = $1, \"name\" = $2, \"access_key\" = $3", part);
		}
	}
	
	public sealed class SetSnippetTest
	{
		[Fact]
		public void Parse()
		{
			var part = SnippetRunner.Run(new SetSnippet<Book>(new Book(5, BookState.Finished, "Harry Potter") {AccessKey = "abcd"}), out var ctx);
			var parameters = ctx.Parameters.ToArray();

			Assert.Equal("\"state\" = $1, \"name\" = $2, \"access_key\" = $3", part);
			Assert.Equal(3, parameters.Length);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal(BookState.Finished, parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("Harry Potter", parameters[1].Value);
			Assert.Equal("", parameters[2].ParameterName);
			Assert.Equal("abcd", parameters[2].Value);
		}
		
		[Fact]
		public void Parse_WithNull()
		{
			var part = SnippetRunner.Run(new SetSnippet<Book>(new Book(5, BookState.InProgress, "Harry Potter")), out var ctx);
			var parameters = ctx.Parameters.ToArray();

			Assert.Equal("\"state\" = $1, \"name\" = $2, \"access_key\" = $3", part);
			Assert.Equal(3, parameters.Length);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal(BookState.InProgress, parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("Harry Potter", parameters[1].Value);
			Assert.Equal("", parameters[2].ParameterName);
			Assert.Same(DBNull.Value, parameters[2].Value);
		}
	}
}
