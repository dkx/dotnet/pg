﻿using System.Linq;
using DKX.Pg.Snippets;
using DKX.Pg.Tests.Utils;
using Xunit;

namespace DKX.Pg.Tests.Snippets
{
	public sealed class ParameterSnippetConnectionExtensionsTest
	{
		[Fact]
		public void Param()
		{
			var part = SnippetRunner.Run(connection => connection.Param("Harry %"), out var ctx);
			Assert.Equal("$1", part);
		}
	}
	
	public sealed class ParameterSnippetTest
	{
		[Fact]
		public void Parse()
		{
			var part = SnippetRunner.Run(new ParameterSnippet("Harry %"), out var ctx);

			Assert.Equal("$1", part);
			Assert.Equal(1, ctx.Parameters.Count);
			Assert.Equal("", ctx.Parameters.First().ParameterName);
			Assert.Equal("Harry %", ctx.Parameters.First().Value);
		}
	}
}
