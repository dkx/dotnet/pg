﻿using System.Linq;
using DKX.Pg.Snippets;
using DKX.Pg.Tests.Utils;
using Xunit;

namespace DKX.Pg.Tests.Snippets
{
	public sealed class IfSnippetConnectionExtensionsTest
	{
		[Fact]
		public void If()
		{
			var part = SnippetRunner.Run(connection => connection.If(true, $"WHERE id = {3}"), out var ctx);
			Assert.Equal("WHERE id = $1", part);
		}
	}
	
	public sealed class IfSnippetTest
	{
		[Fact]
		public void Parse_True()
		{
			var part = SnippetRunner.Run(new IfSnippet(true, $"WHERE id = {3}", $"WHERE id != {5}"), out var ctx);

			Assert.Equal("WHERE id = $1", part);
			Assert.Equal(1, ctx.Parameters.Count);
			Assert.Equal("", ctx.Parameters.First().ParameterName);
			Assert.Equal(3, ctx.Parameters.First().Value);
		}
		
		[Fact]
		public void Parse_False()
		{
			var part = SnippetRunner.Run(new IfSnippet(false, $"WHERE id = {3}", $"WHERE id != {5}"), out var ctx);

			Assert.Equal("WHERE id != $1", part);
			Assert.Equal(1, ctx.Parameters.Count);
			Assert.Equal("", ctx.Parameters.First().ParameterName);
			Assert.Equal(5, ctx.Parameters.First().Value);
		}
		
		[Fact]
		public void Parse_FalseWithoutElse()
		{
			var part = SnippetRunner.Run(new IfSnippet(false, $"WHERE id = {3}"), out var ctx);

			Assert.Equal("", part);
			Assert.Equal(0, ctx.Parameters.Count);
		}
	}
}
