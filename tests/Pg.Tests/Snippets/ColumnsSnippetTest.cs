﻿using DKX.Pg.Snippets;
using DKX.Pg.Tests.Data;
using DKX.Pg.Tests.Utils;
using Xunit;

namespace DKX.Pg.Tests.Snippets
{
	public sealed class ColumnsSnippetConnectionExtensionsTest
	{
		[Fact]
		public void Columns()
		{
			var part = SnippetRunner.Run(connection => connection.Columns<Book>("b"), out var ctx);
			Assert.Equal("b.\"id\", b.\"state\", b.\"name\", b.\"access_key\"", part);
		}
	}
	
	public sealed class ColumnsSnippetTest
	{
		[Theory]
		[InlineData(null, "\"id\", \"state\", \"name\", \"access_key\"")]
		[InlineData("b", "b.\"id\", b.\"state\", b.\"name\", b.\"access_key\"")]
		public void Process(string? alias, string expected)
		{
			var part = SnippetRunner.Run(new ColumnsSnippet<Book>(alias), out var ctx);

			Assert.Equal(expected, part);
			Assert.Equal(0, ctx.Parameters.Count);
		}
	}
}
