using System;
using DKX.Pg.Entities;

namespace DKX.Pg.Tests.Data
{
	[Entity]
	public partial class Location
	{
		public Location()
		{
		}
		
		[Id]
		public Guid Id { get; set; }
		
		public Guid? RootId { get; set; }

		public Guid? ParentId { get; set; }

		public Guid? DistrictId { get; set; }

		public Guid? RegionTownId { get; set; }

		public LocationLevel Level { get; set; }

		public string Name { get; set; }

		public string? Code { get; set; }

		public string? AlternativeCode { get; set; }

		public DateTime CreatedAt { get; set; }

		public DateTime UpdatedAt { get; set; }

		public DateTime? DisabledAt { get; set; }
	}
}
