﻿using System;
using System.Collections.Generic;
using DKX.Pg.Metadata;

namespace DKX.Pg.Tests.Data
{
	public sealed class StaticEntitiesMetadataProvider : IEntitiesMetadataProvider
	{
		private readonly IEnumerable<Type> _types;

		public StaticEntitiesMetadataProvider(IEnumerable<Type> types)
		{
			_types = types;
		}

		public IEnumerable<Type> GetEntitiesMetadataTypes()
		{
			return _types;
		}
	}
}
