﻿using System;
using System.Collections.Generic;
using DKX.Pg.Events;
using DKX.Pg.Queries;

namespace DKX.Pg.Tests.Data
{
	public sealed class LoggingEventListener : EventListener
	{
		private readonly string _name;
			
		private readonly IList<string> _logs;

		public LoggingEventListener(string name, IList<string> logs)
		{
			_name = name;
			_logs = logs;
		}

		public override void OnQueryStart(IQuery query)
		{
			_logs.Add($"{_name}: OnQueryStart");
		}

		public override void OnQueryStop(IQuery query, Exception? exception)
		{
			_logs.Add($"{_name}: OnQueryStop{(exception == null ? "" : $" (exception: {exception.Message})")}");
		}
	}
}
