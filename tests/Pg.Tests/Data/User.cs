﻿using DKX.Pg.Entities;

namespace DKX.Pg.Tests.Data
{
	[Entity]
	public partial class User
	{
		[Id, Generated]
		public int Id { get; }
		
		public string Email { get; }
	}
}
