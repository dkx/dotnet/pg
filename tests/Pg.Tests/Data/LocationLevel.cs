namespace DKX.Pg.Tests.Data
{
	public enum LocationLevel
	{
		Country,
		CountryPart,
		Region,
		District,
		Town,
		TownPart,
	}
}
