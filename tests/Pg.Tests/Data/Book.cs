﻿using DKX.Pg.Entities;

namespace DKX.Pg.Tests.Data
{
	[Entity]
	public partial class Book
	{
		public Book(int id, BookState state, string name)
		{
			Id = id;
			State = state;
			Name = name;
		}

		[Id, Generated]
		public int Id { get; }
		
		public BookState State { get; }
		
		public string Name { get; }
		
		public string? AccessKey { get; set; }
	}
}
