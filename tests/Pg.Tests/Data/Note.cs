﻿using System;
using DKX.Pg.Entities;

namespace DKX.Pg.Tests.Data
{
	[Entity]
	public partial class Note
	{
		public Note(Guid id, string content)
		{
			Id = id;
			Content = content;
		}

		[Id]
		public Guid Id { get; }
		
		public string Content { get; }

		public bool? State { get; set; }
	}
}
