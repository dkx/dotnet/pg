﻿using NpgsqlTypes;

namespace DKX.Pg.Tests.Data
{
	public enum BookState
	{
		[PgName("in_progress")] InProgress,
		[PgName("discontinued")] Discontinued,
		[PgName("finished")] Finished,
	}
}
