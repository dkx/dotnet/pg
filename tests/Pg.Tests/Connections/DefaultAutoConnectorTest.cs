﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using DKX.Pg.Connections;
using Xunit;

namespace DKX.Pg.Tests.Connections
{
	public sealed class DefaultAutoConnectorTest : DatabaseTestCase
	{
		private readonly IList<ConnectionState> _states = new List<ConnectionState>();

		public DefaultAutoConnectorTest()
		{
			_states.Add(Connection.State);
			Connection.StateChange += (sender, args) => _states.Add(args.CurrentState);
		}

		[Fact]
		public async Task Open()
		{
			var autoConnector = new DefaultAutoConnector();
			
			Assert.Equal(new[]
			{
				ConnectionState.Closed,
			}, _states);

			await autoConnector.Open(Connection);
			
			Assert.Equal(new[]
			{
				ConnectionState.Closed,
				ConnectionState.Open,
			}, _states);

			await autoConnector.Open(Connection);
			
			Assert.Equal(new[]
			{
				ConnectionState.Closed,
				ConnectionState.Open,
			}, _states);

			await Connection.CloseAsync();
			
			Assert.Equal(new[]
			{
				ConnectionState.Closed,
				ConnectionState.Open,
				ConnectionState.Closed,
			}, _states);

			await autoConnector.Open(Connection);
			
			Assert.Equal(new[]
			{
				ConnectionState.Closed,
				ConnectionState.Open,
				ConnectionState.Closed,
				ConnectionState.Open,
			}, _states);
		}
	}
}
