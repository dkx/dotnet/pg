﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using DKX.Pg.Exceptions;
using DKX.Pg.Snippets;
using DKX.Pg.Tests.Data;
using Xunit;

namespace DKX.Pg.Tests
{
	public sealed class ConnectionTest : DatabaseTestCase
	{
		[Fact]
		public async Task AddEventListener()
		{
			var logs = new List<string>();
			Db.AddEventListener(new LoggingEventListener("a", logs));

			var select = Db.Query($"SELECT 1").GetSingleScalarResult<int>();
			Assert.Equal(new[]
			{
				"a: OnQueryStart",
			}, logs);

			await select;
			Assert.Equal(new[]
			{
				"a: OnQueryStart",
				"a: OnQueryStop",
			}, logs);
		}

		[Fact]
		public async Task TryOpen_WithAutoOpen()
		{
			await using var db = new Connection(Connection, MetadataProvider, new ConnectionOptions
			{
				AutoOpen = true,
			});

			Assert.Equal(ConnectionState.Closed, Connection.State);

			await db.TryOpen();
			await db.TryOpen();
			await db.TryOpen();

			Assert.Equal(ConnectionState.Open, Connection.State);
		}

		[Fact]
		public async Task TryOpen_WithoutAutoOpen()
		{
			await using var db = new Connection(Connection, MetadataProvider, new ConnectionOptions
			{
				AutoOpen = false,
			});

			Assert.Equal(ConnectionState.Closed, Connection.State);

			await db.TryOpen();
			await db.TryOpen();
			await db.TryOpen();

			Assert.Equal(ConnectionState.Open, Connection.State);
		}

		[Fact]
		public async Task OpenClose()
		{
			Assert.Equal(ConnectionState.Closed, Connection.State);
			await Db.Open();
			Assert.Equal(ConnectionState.Open, Connection.State);
			await Db.Close();
			Assert.Equal(ConnectionState.Closed, Connection.State);
		}

		[Fact]
		public void Query()
		{
			var query = Db.Query($"SELECT * FROM books WHERE id = {3}");
			var parameters = query.Parameters.ToArray();
			
			Assert.Equal("SELECT * FROM books WHERE id = $1", query.Sql);
			Assert.Single(parameters);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal(3, parameters[0].Value);
		}

		[Fact]
		public void CreateInsertQuery()
		{
			var id = Guid.NewGuid();
			var query = Db.CreateInsertQuery("notes", new Note(id, "lorem ipsum") { State = true });
			var parameters = query.Parameters.ToArray();
			
			Assert.Equal("INSERT INTO \"notes\" (\"id\", \"content\", \"state\") VALUES ($1, $2, $3)", query.Sql);
			Assert.Equal(3, parameters.Length);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal(id, parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("lorem ipsum", parameters[1].Value);
			Assert.Equal("", parameters[2].ParameterName);
			Assert.Equal(true, parameters[2].Value);
		}

		[Fact]
		public void CreateInsertQuery_WithGeneratedIds()
		{
			var query = Db.CreateInsertQuery("books", new Book(5, BookState.Finished, "Harry Potter") {AccessKey = "abcd"});
			var parameters = query.Parameters.ToArray();
			
			Assert.Equal("INSERT INTO \"books\" (\"state\", \"name\", \"access_key\") VALUES ($1, $2, $3)", query.Sql);
			Assert.Equal(3, parameters.Length);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal(BookState.Finished, parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("Harry Potter", parameters[1].Value);
			Assert.Equal("", parameters[2].ParameterName);
			Assert.Equal("abcd", parameters[2].Value);
		}

		[Fact]
		public async Task Insert()
		{
			Assert.Equal(0, await Db.Query($"SELECT COUNT(*) FROM notes").GetSingleScalarResult<long>());

			var id = Guid.NewGuid();
			await Db.Insert("notes", new Note(id, "hello"));
			
			Assert.Equal(1, await Db.Query($"SELECT COUNT(*) FROM notes").GetSingleScalarResult<long>());
			Assert.Equal(id, await Db.Query($"SELECT id FROM notes").GetSingleScalarResult<Guid>());
			Assert.Equal("hello", await Db.Query($"SELECT content FROM notes").GetSingleScalarResult<string>());
		}

		[Fact]
		public async Task Insert_WithGeneratedIds()
		{
			Assert.Equal(2, await Db.Query($"SELECT COUNT(*) FROM books").GetSingleScalarResult<long>());

			await Db.Insert("books", new Book(default!, BookState.Discontinued, "Lorem"));
			
			Assert.Equal(3, await Db.Query($"SELECT COUNT(*) FROM books").GetSingleScalarResult<long>());
			Assert.Equal(3, await Db.Query($"SELECT id FROM books ORDER BY id DESC LIMIT 1").GetSingleScalarResult<int>());
		}

		[Fact]
		public void CreateUpdateQuery()
		{
			var query = Db.CreateUpdateQuery("books", new Book(5, BookState.InProgress, "Harry Potter") {AccessKey = "abcd"});
			var parameters = query.Parameters.ToArray();
			
			Assert.Equal("UPDATE \"books\" SET \"state\" = $1, \"name\" = $2, \"access_key\" = $3 WHERE (\"id\" = $4)", query.Sql);
			Assert.Equal(4, parameters.Length);
			Assert.Equal("", parameters[0].ParameterName);
			Assert.Equal(BookState.InProgress, parameters[0].Value);
			Assert.Equal("", parameters[1].ParameterName);
			Assert.Equal("Harry Potter", parameters[1].Value);
			Assert.Equal("", parameters[2].ParameterName);
			Assert.Equal("abcd", parameters[2].Value);
			Assert.Equal("", parameters[3].ParameterName);
			Assert.Equal(5, parameters[3].Value);
		}

		[Fact]
		public async Task Update()
		{
			var id = Guid.NewGuid();
			await Db.Insert("notes", new Note(id, "hello"));
			
			Assert.Equal(1, await Db.Query($"SELECT COUNT(*) FROM notes").GetSingleScalarResult<long>());
			Assert.Equal(id, await Db.Query($"SELECT id FROM notes").GetSingleScalarResult<Guid>());
			Assert.Equal("hello", await Db.Query($"SELECT content FROM notes").GetSingleScalarResult<string>());

			var affected = await Db.Update("notes", new Note(id, "hello pg"));
			Assert.Equal(1, affected);
			
			Assert.Equal(1, await Db.Query($"SELECT COUNT(*) FROM notes").GetSingleScalarResult<long>());
			Assert.Equal(id, await Db.Query($"SELECT id FROM notes").GetSingleScalarResult<Guid>());
			Assert.Equal("hello pg", await Db.Query($"SELECT content FROM notes").GetSingleScalarResult<string>());
		}

		[Fact]
		public void Update_ThrowsNoIds()
		{
			var e = Assert.Throws<QueryException>(() => Db.CreateUpdateQuery("empty", new EmptyEntity()));
			Assert.Equal("Can not update entity DKX.Pg.Tests.Data.EmptyEntity because it does not have any ids", e.Message);
		}
		
		[Fact]
		public async Task Transactional()
		{
			await Db.Transactional(async () =>
			{
				await Db.Query($"INSERT INTO notes (id, content) VALUES (uuid_generate_v4(), 'note 01')").Execute();
			});

			try
			{
				await Db.Transactional(async () =>
				{
					await Db.Query($"INSERT INTO notes (id, content) VALUES (uuid_generate_v4(), 'note 02')").Execute();
					throw new Exception();
				});
			}
			catch
			{
				// ignored
			}

			var userId = await Db.Transactional(async () =>
			{
				await Db.Query($"INSERT INTO notes (id, content) VALUES (uuid_generate_v4(), 'note 03')").Execute();
				return 3;
			});

			Assert.Equal(3, userId);

			try
			{
				await Db.Transactional<int>(async () =>
				{
					await Db.Query($"INSERT INTO notes (id, content) VALUES (uuid_generate_v4(), 'note 04')").Execute();
					throw new Exception();
				});
			}
			catch
			{
				// ignored
			}

			var notes = await Db.Query($"SELECT content FROM notes ORDER BY content").GetScalarResult<string>().ToArrayAsync();
			
			Assert.Equal(new[]
			{
				"note 01",
				"note 03",
			}, notes);
		}

		[Fact]
		public async Task Transactional_Nested()
		{
			await Db.Transactional(async () =>
			{
				await Db.Transactional(async () =>
				{
					await Db.Transactional(async () =>
					{
						await Db.Query($"INSERT INTO notes (id, content) VALUES (uuid_generate_v4(), 'note 01')").Execute();
					});
					
					await Db.Query($"INSERT INTO notes (id, content) VALUES (uuid_generate_v4(), 'note 02')").Execute();
					
					await Db.Transactional(async () =>
					{
						await Db.Transactional(async () =>
						{
							await Db.Query($"INSERT INTO notes (id, content) VALUES (uuid_generate_v4(), 'note 03')").Execute();
						});
					});
				});

				await Db.Transactional(async () =>
				{
					await Db.Transactional(async () =>
					{
						await Db.Query($"INSERT INTO notes (id, content) VALUES (uuid_generate_v4(), 'note 04')").Execute();
					});
					
					await Db.Query($"INSERT INTO notes (id, content) VALUES (uuid_generate_v4(), 'note 05')").Execute();
					
					await Db.Transactional(async () =>
					{
						await Db.Query($"INSERT INTO notes (id, content) VALUES (uuid_generate_v4(), 'note 06')").Execute();
					});
				});
			});

			var notes = await Db.Query($"SELECT content FROM notes ORDER BY content").GetScalarResult<string>().ToArrayAsync();
			
			Assert.Equal(new[]
			{
				"note 01",
				"note 02",
				"note 03",
				"note 04",
				"note 05",
				"note 06",
			}, notes);
		}

		[Fact]
		public async Task InsertAndFetchComplexType()
		{
			var now = DateTime.Today;
			var create = new Location
			{
				Id = Guid.NewGuid(),
				ParentId = null,
				DistrictId = Guid.NewGuid(),
				RegionTownId = null,
				Level = LocationLevel.Town,
				Name = "Random Town",
				Code = "00102",
				AlternativeCode = "20100",
				CreatedAt = now,
				UpdatedAt = now,
				DisabledAt = null,
			};

			await Db.Insert("location", create);

			var location = await Db
				.Query($"SELECT {Db.Columns<Location>()} FROM location LIMIT 1")
				.GetSingleResult<Location>();

			Assert.Equal(create.Id, location.Id);
			Assert.Equal(create.RootId, location.RootId);
			Assert.Equal(create.ParentId, location.ParentId);
			Assert.Equal(create.DistrictId, location.DistrictId);
			Assert.Equal(create.RegionTownId, location.RegionTownId);
			Assert.Equal(create.Level, location.Level);
			Assert.Equal(create.Name, location.Name);
			Assert.Equal(create.Code, location.Code);
			Assert.Equal(create.AlternativeCode, location.AlternativeCode);
			Assert.Equal(create.CreatedAt, location.CreatedAt);
			Assert.Equal(create.UpdatedAt, location.UpdatedAt);
			Assert.Equal(create.DisabledAt, location.DisabledAt);
		}
	}
}
