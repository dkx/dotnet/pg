using System;
using System.Data;
using DKX.Pg.Metadata;
using DKX.Pg.Tests.Data;
using Npgsql;

namespace DKX.Pg.Tests
{
	public abstract class DatabaseTestCase : IDisposable
	{
		protected readonly NpgsqlConnection Connection;

		protected readonly IEntitiesMetadataProvider MetadataProvider;
		
		protected readonly Connection Db;

		protected DatabaseTestCase()
		{
			var host = Environment.GetEnvironmentVariable("POSTGRES_HOST") ?? "localhost";
			var id = "test_" + Guid.NewGuid().ToString().Replace("-", "");

			CreateDatabase(host, id);

			Connection = new NpgsqlConnection(CreateConnectionString(host, id));
			Connection.StateChange += (sender, args) =>
			{
				if (args.CurrentState == ConnectionState.Open)
				{
					Connection.ReloadTypes();
					Connection.TypeMapper.MapEnum<BookState>("book_state");
					Connection.TypeMapper.MapEnum<LocationLevel>("location_level");
				}
			};

			MetadataProvider = new AssemblyEntitiesMetadataProvider(GetType().Assembly);
			Db = new Connection(Connection, MetadataProvider);
		}

		public void Dispose()
		{
			Db.Dispose();
		}

		private static string CreateConnectionString(string host, string database)
		{
			return $"Host={host};Username=postgres;Password=12345;Database={database};";
		}

		private static void CreateDatabase(string host, string id)
		{
			// create new unique database for test
			using (var connection = new NpgsqlConnection(CreateConnectionString(host, "postgres")))
			{
				connection.Open();

				using (var command = connection.CreateCommand())
				{
					command.CommandText = $"CREATE DATABASE {id}";
					command.ExecuteNonQuery();
				}
			}
			
			// prepare database
			using (var connection = new NpgsqlConnection(CreateConnectionString(host, id)))
			{
				connection.Open();

				using (var command = connection.CreateCommand())
				{
					var sql = @"
CREATE EXTENSION IF NOT EXISTS ""uuid-ossp"";

CREATE TABLE notes (id UUID PRIMARY KEY, content TEXT NOT NULL, state BOOLEAN DEFAULT NULL);

CREATE TYPE book_state AS ENUM ('in_progress', 'discontinued', 'finished');
CREATE TABLE books (id SERIAL PRIMARY KEY, state book_state NOT NULL, name TEXT NOT NULL, access_key TEXT DEFAULT NULL);

CREATE TYPE location_level AS ENUM ('country', 'country_part', 'region', 'district', 'town', 'town_part');
CREATE TABLE location
(
    id UUID NOT NULL PRIMARY KEY,
    root_id UUID DEFAULT NULL,
    parent_id UUID DEFAULT NULL,
    district_id UUID DEFAULT NULL,
    region_town_id UUID DEFAULT NULL,
    level location_level NOT NULL,
    name VARCHAR(255) NOT NULL,
    code VARCHAR(255) DEFAULT NULL,
    alternative_code VARCHAR(255) DEFAULT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    disabled_at TIMESTAMP DEFAULT NULL
);

INSERT INTO books (state, name, access_key) VALUES
	('discontinued', 'Harry Potter', 'abcd12345'),
	('in_progress', 'Lord of the Rings', null);
";

					command.CommandText = sql;
					command.ExecuteNonQuery();
				}
			}
		}
	}
}
