﻿using System;
using System.Threading.Tasks;
using Xunit;

namespace DKX.Pg.Tests.Queries
{
	public sealed class QueryGetSingleScalarResultTest : BaseQueryTest
	{
		[Fact]
		public async Task GetScalarResult()
		{
			var name = await Db
				.Query($"SELECT name FROM books WHERE id = 1")
				.GetSingleScalarResult<string>();

			Assert.Equal("Harry Potter", name);
		}
		
		[Fact]
		public async Task GetScalarResult_WithNullable()
		{
			var name = await Db
				.Query($"SELECT access_key FROM books WHERE id = 2")
				.GetSingleScalarResult<string?>();

			Assert.Null(name);
		}
		
		[Fact]
		public async Task LogEvents()
		{
			Assert.Empty(Logs);
			
			await Db.Query($"SELECT name FROM books WHERE id = 1").GetSingleScalarResult<string>();

			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
				"logs: OnQueryStop",
			}, Logs);
		}
		
		[Fact]
		public async Task LogEvents_WithError()
		{
			Assert.Empty(Logs);
			
			var query = Db.Query(ErrorSql).GetSingleScalarResult<string>();
			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
			}, Logs);

			try
			{
				await query;
			}
			catch (Exception)
			{
				// ignored
			}

			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
				"logs: OnQueryStop (exception: P0001: Error)",
			}, Logs);
		}
	}
}
