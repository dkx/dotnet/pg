﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DKX.Pg.Exceptions;
using DKX.Pg.Tests.Data;
using Xunit;

namespace DKX.Pg.Tests.Queries
{
	public sealed class QueryGetScalarResultTest : BaseQueryTest
	{
		[Fact]
		public async Task GetScalarResult_Throws_MoreColumnsThanOne()
		{
			var query = Db.Query($"SELECT id, name FROM books");
			var e = await Assert.ThrowsAsync<NonUniqueResultException>(async () =>
			{
				await query.GetScalarResult<string>().ToArrayAsync();
			});

			Assert.Equal("More than one column was found for query, but exactly one was expected", e.Message);
			Assert.Same(query, e.Query);
		}
		
		[Fact]
		public async Task GetScalarResult()
		{
			var data = await Db
				.Query($"SELECT name FROM books ORDER BY id")
				.GetScalarResult<string>()
				.ToArrayAsync();

			Assert.Equal(new[]
			{
				"Harry Potter",
				"Lord of the Rings",
			}, data);
		}
		
		[Fact]
		public async Task GetScalarResult_WithNullable()
		{
			var data = await Db
				.Query($"SELECT access_key FROM books ORDER BY id")
				.GetScalarResult<string?>()
				.ToArrayAsync();

			Assert.Equal(new[]
			{
				"abcd12345",
				null,
			}, data);
		}
		
		[Fact]
		public async Task LogEvents()
		{
			Assert.Empty(Logs);
			var result = Db.Query($"SELECT name FROM books").GetScalarResult<string>();
			Assert.Empty(Logs);

			await foreach (var name in result)
			{
				Assert.Equal(new[]
				{
					"logs: OnQueryStart",
					"logs: OnQueryStop",
				}, Logs);
			}

			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
				"logs: OnQueryStop",
			}, Logs);
		}
		
		[Fact]
		public async Task LogEvents_WithError()
		{
			Assert.Empty(Logs);
			var result = Db.Query(ErrorSql).GetScalarResult<string>();
			Assert.Empty(Logs);

			try
			{
				await result.ToArrayAsync();
			}
			catch (Exception)
			{
				// ignored
			}

			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
				"logs: OnQueryStop (exception: P0001: Error)",
			}, Logs);
		}
	}
}
