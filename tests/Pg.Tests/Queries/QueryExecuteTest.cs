﻿using System;
using System.Threading.Tasks;
using Xunit;

namespace DKX.Pg.Tests.Queries
{
	public sealed class QueryExecuteTest : BaseQueryTest
	{
		[Fact]
		public async Task Execute()
		{
			var rows = await Db
				.Query($"DELETE FROM books")
				.Execute();

			Assert.Equal(2, rows);
		}
		
		[Fact]
		public async Task LogEvents()
		{
			Assert.Empty(Logs);
			
			await Db.Query($"SELECT 1").Execute();

			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
				"logs: OnQueryStop",
			}, Logs);
		}
		
		[Fact]
		public async Task LogEvents_WithError()
		{
			Assert.Empty(Logs);

			try
			{
				await Db.Query(ErrorSql).Execute();
			}
			catch (Exception)
			{
				// ignored
			}

			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
				"logs: OnQueryStop (exception: P0001: Error)",
			}, Logs);
		}
	}
}
