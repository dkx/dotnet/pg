﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DKX.Pg.Tests.Data;
using Xunit;

namespace DKX.Pg.Tests.Queries
{
	public sealed class QueryGetResultTest : BaseQueryTest
	{
		[Fact]
		public async Task GetResult()
		{
			var books = await Db
				.Query($"SELECT id, state, name, access_key FROM books ORDER BY id")
				.GetResult<Book>()
				.ToArrayAsync();

			Assert.Equal(2, books.Length);
			Assert.Equal(1, books[0].Id);
			Assert.Equal(BookState.Discontinued, books[0].State);
			Assert.Equal("Harry Potter", books[0].Name);
			Assert.Equal("abcd12345", books[0].AccessKey);
			Assert.Equal(2, books[1].Id);
			Assert.Equal(BookState.InProgress, books[1].State);
			Assert.Equal("Lord of the Rings", books[1].Name);
			Assert.Null(books[1].AccessKey);
		}
		
		[Fact]
		public async Task LogEvents()
		{
			Assert.Empty(Logs);
			var result = Db.Query($"SELECT * FROM books").GetResult<Book>();
			Assert.Empty(Logs);

			await foreach (var book in result)
			{
				Assert.Equal(new[]
				{
					"logs: OnQueryStart",
					"logs: OnQueryStop",
				}, Logs);
			}

			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
				"logs: OnQueryStop",
			}, Logs);
		}
		
		[Fact]
		public async Task LogEvents_WithError()
		{
			Assert.Empty(Logs);
			var result = Db.Query(ErrorSql).GetResult<Book>();
			Assert.Empty(Logs);

			try
			{
				await result.ToArrayAsync();
			}
			catch (Exception)
			{
				// ignored
			}

			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
				"logs: OnQueryStop (exception: P0001: Error)",
			}, Logs);
		}
	}
}
