﻿using System;
using System.Collections.Generic;
using DKX.Pg.Tests.Data;

namespace DKX.Pg.Tests.Queries
{
	public abstract class BaseQueryTest : DatabaseTestCase
	{
		protected readonly IList<string> Logs = new List<string>();

		protected readonly FormattableString ErrorSql = $"DO $$ BEGIN RAISE EXCEPTION 'Error'; END $$";

		protected BaseQueryTest()
		{
			Db.AddEventListener(new LoggingEventListener("logs", Logs));
		}
	}
}
