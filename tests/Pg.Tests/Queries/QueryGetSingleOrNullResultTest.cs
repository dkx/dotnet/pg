﻿using System;
using System.Threading.Tasks;
using DKX.Pg.Exceptions;
using DKX.Pg.Tests.Data;
using Xunit;

namespace DKX.Pg.Tests.Queries
{
	public sealed class QueryGetSingleOrNullResultTest : BaseQueryTest
	{
		[Fact]
		public async Task GetSingleOrNullResult_Throws_MultipleResults()
		{
			var query = Db.Query($"SELECT id, state, name, access_key FROM books");
			var e = await Assert.ThrowsAsync<NonUniqueResultException>(async () =>
			{
				await query.GetSingleOrNullResult<Book>();
			});

			Assert.Equal("More than one result was found for query, but only one or none was expected", e.Message);
			Assert.Same(query, e.Query);
		}
		
		[Fact]
		public async Task GetSingleOrNullResult()
		{
			var book = await Db
				.Query($"SELECT id, state, name, access_key FROM books WHERE id = 2")
				.GetSingleOrNullResult<Book>();

			Assert.NotNull(book);
			Assert.Equal(2, book!.Id);
			Assert.Equal("Lord of the Rings", book!.Name);
			Assert.Null(book!.AccessKey);
		}
		
		[Fact]
		public async Task LogEvents()
		{
			Assert.Empty(Logs);
			
			await Db.Query($"SELECT * FROM books WHERE id = 1").GetSingleOrNullResult<Book>();

			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
				"logs: OnQueryStop",
			}, Logs);
		}
		
		[Fact]
		public async Task LogEvents_WithError()
		{
			Assert.Empty(Logs);
			
			try
			{
				await Db.Query(ErrorSql).GetSingleOrNullResult<Book>();
			}
			catch (Exception)
			{
				// ignored
			}

			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
				"logs: OnQueryStop (exception: P0001: Error)",
			}, Logs);
		}
	}
}
