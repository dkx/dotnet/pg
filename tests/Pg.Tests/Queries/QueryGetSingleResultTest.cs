﻿using System;
using System.Threading.Tasks;
using DKX.Pg.Exceptions;
using DKX.Pg.Tests.Data;
using Xunit;

namespace DKX.Pg.Tests.Queries
{
	public sealed class QueryGetSingleResultTest : BaseQueryTest
	{
		[Fact]
		public async Task GetSingleResult()
		{
			var book = await Db
				.Query($"SELECT id, state, name, access_key FROM books WHERE id = 2")
				.GetSingleResult<Book>();

			Assert.NotNull(book);
			Assert.Equal(2, book.Id);
			Assert.Equal("Lord of the Rings", book.Name);
			Assert.Null(book.AccessKey);
		}
		
		[Fact]
		public async Task GetSingleResult_Throw_Null()
		{
			var query = Db.Query($"SELECT id, name FROM books WHERE id = 5");
			var e = await Assert.ThrowsAsync<NoResultException>(async () =>
			{
				await query.GetSingleResult<Book>();
			});

			Assert.Equal("No result was found for query, but exactly one was expected", e.Message);
			Assert.Same(query, e.Query);
		}
		
		[Fact]
		public async Task GetSingleResult_RandomizeSelectColumns()
		{
			var book = await Db
				.Query($"SELECT access_key, id, state, name FROM books WHERE id = 2")
				.GetSingleResult<Book>();

			Assert.NotNull(book);
			Assert.Equal(2, book.Id);
			Assert.Equal("Lord of the Rings", book.Name);
			Assert.Null(book.AccessKey);
		}
		
		[Fact]
		public async Task LogEvents()
		{
			Assert.Empty(Logs);
			
			await Db.Query($"SELECT * FROM books WHERE id = 1").GetSingleResult<Book>();

			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
				"logs: OnQueryStop",
			}, Logs);
		}
		
		[Fact]
		public async Task LogEvents_WithError()
		{
			Assert.Empty(Logs);

			try
			{
				await Db.Query(ErrorSql).GetSingleResult<Book>();
			}
			catch (Exception)
			{
				// ignored
			}

			Assert.Equal(new[]
			{
				"logs: OnQueryStart",
				"logs: OnQueryStop (exception: P0001: Error)",
			}, Logs);
		}
	}
}
