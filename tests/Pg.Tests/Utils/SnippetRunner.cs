﻿using System;
using DKX.Pg.Metadata;
using DKX.Pg.Parsing;
using DKX.Pg.Snippets;
using Moq;

namespace DKX.Pg.Tests.Utils
{
	internal static class SnippetRunner
	{
		public static string Run(ISnippet snippet, out QueryParsingContext ctx)
		{
			var parser = new SqlParser();
			var mappingsRegistry = new EntitiesMetadataRegistry(new AssemblyEntitiesMetadataProvider(typeof(SnippetRunner).Assembly));

			ctx = new QueryParsingContext(parser, mappingsRegistry, false);
			
			return snippet.Process(ctx, null);
		}

		public static string Run(Func<IConnection, ISnippet> fn, out QueryParsingContext ctx)
		{
			return Run(fn(new Mock<IConnection>().Object), out ctx);
		}
	}
}
