﻿using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.Extensions.FileSystemGlobbing;
using Microsoft.Extensions.FileSystemGlobbing.Abstractions;
using Xunit;

namespace DKX.Pg.AOT.Tests
{
	public static class Helpers
	{
		public static async Task VerifySourceGenerator<TSourceGenerator>(string kind, string sampleName, DiagnosticInfo[]? expectedDiagnostics = null)
			where TSourceGenerator : ISourceGenerator, new()
		{
			var input = await File.ReadAllTextAsync(Path.Combine("Samples", kind, $"{sampleName}.input.cs"), Encoding.UTF8);

			var inputCompilation = CreateCompilation(input);
			var generator = new TSourceGenerator();
			GeneratorDriver driver = CSharpGeneratorDriver.Create(generator);

			driver = driver.RunGeneratorsAndUpdateCompilation(inputCompilation, out var outputCompilation, out var diagnostics);

			var matcher = new Matcher();
			matcher.AddInclude($"{sampleName}.output_*.cs");

			var outputFiles = matcher.Execute(new DirectoryInfoWrapper(new DirectoryInfo(Path.Combine("Samples", kind)))).Files.OrderBy(f => f.Path).ToArray();

			if (expectedDiagnostics == null || expectedDiagnostics.Length == 0)
			{
				Assert.Empty(diagnostics);
			}
			else
			{
				Assert.Equal(expectedDiagnostics.Length, diagnostics.Length);

				for (var i = 0; i < diagnostics.Length; i++)
				{
					Assert.Equal(expectedDiagnostics[i].Severity, diagnostics[i].DefaultSeverity);
					Assert.Equal(expectedDiagnostics[i].Id, diagnostics[i].Id);
					Assert.Equal(expectedDiagnostics[i].Title, diagnostics[i].Descriptor.Title.ToString());
					Assert.Equal(expectedDiagnostics[i].Message, diagnostics[i].GetMessage());
				}
			}
			
			Assert.Equal(outputFiles.Length + 2, outputCompilation.SyntaxTrees.Count());
			Assert.Empty(outputCompilation.GetDiagnostics());

			var syntaxTrees = outputCompilation.SyntaxTrees.ToArray();

			for (var i = 0; i < outputFiles.Length; i++)
			{
				var outputFile = Path.Combine("Samples", kind, outputFiles[i].Path);
				var output = await File.ReadAllTextAsync(outputFile, Encoding.UTF8);
				var syntaxTree = syntaxTrees[i + 2];

				Assert.Equal(output, syntaxTree.ToString());
			}
		}

		private static Compilation CreateCompilation(string source)
		{
			var program = $@"
namespace DKX.AOT.Tests
{{
	public static class CompiledProgram
	{{
		public static void Main(string[] args)
		{{
		}}
	}}
}}
".TrimStart();

			return CSharpCompilation.Create(
				"compilation",
				new[]
				{
					CSharpSyntaxTree.ParseText(program),
					CSharpSyntaxTree.ParseText(source),
				},
				new[]
				{
					MetadataReference.CreateFromFile(typeof(Binder).GetTypeInfo().Assembly.Location),
					MetadataReference.CreateFromFile(Assembly.Load("System.Runtime").Location),
					MetadataReference.CreateFromFile(Assembly.Load("System.Data").Location),
					MetadataReference.CreateFromFile(Assembly.Load("netstandard").Location),
					MetadataReference.CreateFromFile(typeof(IConnection).GetTypeInfo().Assembly.Location),
					MetadataReference.CreateFromFile(typeof(IDataRecord).GetTypeInfo().Assembly.Location),
				},
				new CSharpCompilationOptions(
					OutputKind.ConsoleApplication,
					nullableContextOptions: NullableContextOptions.Enable
				)
			);
		}
	}

	public sealed class DiagnosticInfo
	{
		public DiagnosticInfo(DiagnosticSeverity severity, string id, string title, string message)
		{
			Severity = severity;
			Id = id;
			Title = title;
			Message = message;
		}

		public DiagnosticSeverity Severity { get; }

		public string Id { get; }
		
		public string Title { get; }
		
		public string Message { get; }
	}
}
