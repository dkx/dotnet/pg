﻿using DKX.Pg.Entities;

namespace DKX.Pg.AOT.Tests.Samples.ValidEntityMappingGeneratorChained
{
	public sealed class GetResult
	{
		private readonly IConnection _db;

		public GetResult(IConnection db)
		{
			_db = db;
		}

		public void Call()
		{
			var result = _db
				.Query($"SELECT * FROM users")
				.GetResult<GetResultEntity>();
		}
	}

	[Entity]
	public partial class GetResultEntity
	{
	}
}
