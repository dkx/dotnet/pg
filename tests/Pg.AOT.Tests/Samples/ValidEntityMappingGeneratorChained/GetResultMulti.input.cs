﻿using DKX.Pg.Entities;

namespace DKX.Pg.AOT.Tests.Samples.ValidEntityMappingGeneratorChained
{
	public sealed class GetResultMulti
	{
		private readonly IConnection _db;

		public GetResultMulti(IConnection db)
		{
			_db = db;
		}

		public void Call()
		{
			var a = _db
				.Query($"SELECT * FROM users")
				.GetResult<GetResultMultiEntityA>();
			
			var b = _db
				.Query($"SELECT * FROM users")
				.GetResult<GetResultMultiEntityB>();
			
			var c = _db
				.Query($"SELECT * FROM users")
				.GetResult<GetResultMultiEntityC>();
			
			var d = _db
				.Query($"SELECT * FROM users")
				.GetResult<GetResultMultiEntityD>();
		}
	}

	[Entity]
	public partial class GetResultMultiEntityA
	{
	}

	public partial class GetResultMultiEntityB
	{
	}

	[Entity]
	public partial class GetResultMultiEntityC
	{
	}

	public partial class GetResultMultiEntityD
	{
	}
}
