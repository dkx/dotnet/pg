﻿namespace DKX.Pg.AOT.Tests.Samples.ValidEntityMappingGeneratorChained
{
	public sealed class GetResultInvalid
	{
		private readonly IConnection _db;

		public GetResultInvalid(IConnection db)
		{
			_db = db;
		}

		public void Call()
		{
			var result = _db
				.Query($"SELECT * FROM users")
				.GetResult<GetResultInvalidEntity>();
		}
	}

	public sealed class GetResultInvalidEntity
	{
	}
}
