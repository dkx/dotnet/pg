﻿namespace DKX.Pg.AOT.Tests.Samples.ValidEntityMappingGeneratorChained
{
	public sealed class GetResultGeneric
	{
		private readonly IConnection _db;

		public GetResultGeneric(IConnection db)
		{
			_db = db;
		}

		public void Call<T>()
			where T : class
		{
			var result = _db
				.Query($"SELECT * FROM users")
				.GetResult<T>();
		}
	}
}
