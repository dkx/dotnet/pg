﻿using DKX.Pg.Entities;

namespace DKX.Pg.AOT.Tests.Samples.EntityGeneratorClasses
{
	public sealed class EntityParent
	{
		[Entity]
		private partial class NotPublic
		{
		}
	}
}
