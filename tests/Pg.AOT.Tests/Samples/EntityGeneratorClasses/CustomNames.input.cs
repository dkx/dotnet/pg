﻿using DKX.Pg.Entities;

namespace DKX.Pg.AOT.Tests.Samples.EntityGeneratorClasses
{
	[Entity]
	public partial class CustomNames
	{
		[Name("full_name")]
		public string Name { get; }
	}
}
