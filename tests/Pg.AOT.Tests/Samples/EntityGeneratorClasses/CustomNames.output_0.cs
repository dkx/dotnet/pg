﻿#nullable enable

//------------------------------------------------------------------------------
// <auto-generated>
// This code was generated by:
//     DKX.Pg.AOT.Generators.EntityGenerator
// Changes to this file may cause incorrect behavior and
// will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DKX.Pg.AOT.Tests.Samples.EntityGeneratorClasses
{
	public partial class CustomNames
	{
		public CustomNames(global::System.Data.IDataRecord record, int[] mapping)
		{
			Name = record.GetString(mapping[0]);
		}
	}

	public sealed class CustomNamesEntityMetadata : global::DKX.Pg.Metadata.IEntityMetadata<CustomNames>
	{
		public global::DKX.Pg.Metadata.IPropertyMetadata<CustomNames>[] Properties { get; } = new global::DKX.Pg.Metadata.IPropertyMetadata<CustomNames>[]
		{
			new global::DKX.Pg.Metadata.PropertyMetadata<CustomNames>("Name", "full_name", false, false, entity => entity.Name),
		};

		public int[] CreateMapping(global::System.Data.IDataRecord record)
		{
			return new int[]
			{
				record.GetOrdinal("full_name"),
			};
		}

		public CustomNames CreateEntity(global::System.Data.IDataRecord record, int[] mapping)
		{
			return new CustomNames(record, mapping);
		}
	}
}
