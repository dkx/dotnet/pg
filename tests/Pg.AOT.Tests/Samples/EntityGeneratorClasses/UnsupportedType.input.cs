﻿using DKX.Pg.Entities;

namespace DKX.Pg.AOT.Tests.Samples.EntityGeneratorClasses
{
	[Entity]
	public partial class UnsupportedType
	{
		public uint Prop { get; }
	}
}
