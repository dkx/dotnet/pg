﻿using System;
using DKX.Pg.Entities;

namespace DKX.Pg.AOT.Tests.Samples.EntityGeneratorClasses
{
	[Entity]
	public partial class PropertiesNotNullable
	{
		[NotMapped]
		public string Gretting => "Hello world";
		
		[Id]
		public int Id { get; }

		public bool BooleanProp { get; }
		
		public byte ByteProp { get; }

		public char CharProp { get; }

		[Generated]
		public DateTime DateTimeProp { get; }

		public decimal DecimalProp { get; }

		public double DoubleProp { get; }

		public float FloatProp { get; }

		public Guid GuidProp { get; }

		public short ShortProp { get; }

		public int IntProp { get; }

		public long LongProp { get; }

		public string StringProp { get; }

		public object ObjectProp { get; }
		
		public PropertiesNotNullableSomeEnum EnumProp { get; }

		public IPropertiesNotNullableSomeComposite CompositeInterfaceProp { get; }
		
		public PropertiesNotNullableSomeComposite CompositeProp { get; }
		
		public DateTime[] ArrayProp { get; }
	}

	public enum PropertiesNotNullableSomeEnum
	{
	}

	public interface IPropertiesNotNullableSomeComposite
	{
	}

	public sealed class PropertiesNotNullableSomeComposite
	{
	}
}
