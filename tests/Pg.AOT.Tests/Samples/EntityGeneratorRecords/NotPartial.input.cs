﻿using DKX.Pg.Entities;

namespace DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords
{
	[Entity]
	public sealed record NotPartial
	{
	}
}
