﻿#nullable enable

//------------------------------------------------------------------------------
// <auto-generated>
// This code was generated by:
//     DKX.Pg.AOT.Generators.EntityGenerator
// Changes to this file may cause incorrect behavior and
// will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords
{
	public partial record PropertiesNullable
	{
		public PropertiesNullable(global::System.Data.IDataRecord record, int[] mapping)
		{
			Id = record.IsDBNull(mapping[0]) ? (int?)null : record.GetInt32(mapping[0]);
			BooleanProp = record.IsDBNull(mapping[1]) ? (bool?)null : record.GetBoolean(mapping[1]);
			ByteProp = record.IsDBNull(mapping[2]) ? (byte?)null : record.GetByte(mapping[2]);
			CharProp = record.IsDBNull(mapping[3]) ? (char?)null : record.GetChar(mapping[3]);
			DateTimeProp = record.IsDBNull(mapping[4]) ? (global::System.DateTime?)null : record.GetDateTime(mapping[4]);
			DecimalProp = record.IsDBNull(mapping[5]) ? (decimal?)null : record.GetDecimal(mapping[5]);
			DoubleProp = record.IsDBNull(mapping[6]) ? (double?)null : record.GetDouble(mapping[6]);
			FloatProp = record.IsDBNull(mapping[7]) ? (float?)null : record.GetFloat(mapping[7]);
			GuidProp = record.IsDBNull(mapping[8]) ? (global::System.Guid?)null : record.GetGuid(mapping[8]);
			ShortProp = record.IsDBNull(mapping[9]) ? (short?)null : record.GetInt16(mapping[9]);
			IntProp = record.IsDBNull(mapping[10]) ? (int?)null : record.GetInt32(mapping[10]);
			LongProp = record.IsDBNull(mapping[11]) ? (long?)null : record.GetInt64(mapping[11]);
			StringProp = record.IsDBNull(mapping[12]) ? (string?)null : record.GetString(mapping[12]);
			ObjectProp = record.IsDBNull(mapping[13]) ? (object?)null : record.GetValue(mapping[13]);
			EnumProp = record.IsDBNull(mapping[14]) ? (global::DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords.PropertiesNullableSomeEnum?)null : (global::DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords.PropertiesNullableSomeEnum)record.GetValue(mapping[14]);
			CompositeInterfaceProp = record.IsDBNull(mapping[15]) ? (global::DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords.IPropertiesNullableSomeComposite?)null : (global::DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords.IPropertiesNullableSomeComposite)record.GetValue(mapping[15]);
			CompositeProp = record.IsDBNull(mapping[16]) ? (global::DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords.PropertiesNullableSomeComposite?)null : (global::DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords.PropertiesNullableSomeComposite)record.GetValue(mapping[16]);
		}
	}

	public sealed class PropertiesNullableEntityMetadata : global::DKX.Pg.Metadata.IEntityMetadata<PropertiesNullable>
	{
		public global::DKX.Pg.Metadata.IPropertyMetadata<PropertiesNullable>[] Properties { get; } = new global::DKX.Pg.Metadata.IPropertyMetadata<PropertiesNullable>[]
		{
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("Id", "id", true, false, entity => entity.Id),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("BooleanProp", "boolean_prop", false, false, entity => entity.BooleanProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("ByteProp", "byte_prop", false, false, entity => entity.ByteProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("CharProp", "char_prop", false, false, entity => entity.CharProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("DateTimeProp", "date_time_prop", false, true, entity => entity.DateTimeProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("DecimalProp", "decimal_prop", false, false, entity => entity.DecimalProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("DoubleProp", "double_prop", false, false, entity => entity.DoubleProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("FloatProp", "float_prop", false, false, entity => entity.FloatProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("GuidProp", "guid_prop", false, false, entity => entity.GuidProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("ShortProp", "short_prop", false, false, entity => entity.ShortProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("IntProp", "int_prop", false, false, entity => entity.IntProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("LongProp", "long_prop", false, false, entity => entity.LongProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("StringProp", "string_prop", false, false, entity => entity.StringProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("ObjectProp", "object_prop", false, false, entity => entity.ObjectProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("EnumProp", "enum_prop", false, false, entity => entity.EnumProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("CompositeInterfaceProp", "composite_interface_prop", false, false, entity => entity.CompositeInterfaceProp),
			new global::DKX.Pg.Metadata.PropertyMetadata<PropertiesNullable>("CompositeProp", "composite_prop", false, false, entity => entity.CompositeProp),
		};

		public int[] CreateMapping(global::System.Data.IDataRecord record)
		{
			return new int[]
			{
				record.GetOrdinal("id"),
				record.GetOrdinal("boolean_prop"),
				record.GetOrdinal("byte_prop"),
				record.GetOrdinal("char_prop"),
				record.GetOrdinal("date_time_prop"),
				record.GetOrdinal("decimal_prop"),
				record.GetOrdinal("double_prop"),
				record.GetOrdinal("float_prop"),
				record.GetOrdinal("guid_prop"),
				record.GetOrdinal("short_prop"),
				record.GetOrdinal("int_prop"),
				record.GetOrdinal("long_prop"),
				record.GetOrdinal("string_prop"),
				record.GetOrdinal("object_prop"),
				record.GetOrdinal("enum_prop"),
				record.GetOrdinal("composite_interface_prop"),
				record.GetOrdinal("composite_prop"),
			};
		}

		public PropertiesNullable CreateEntity(global::System.Data.IDataRecord record, int[] mapping)
		{
			return new PropertiesNullable(record, mapping);
		}
	}
}
