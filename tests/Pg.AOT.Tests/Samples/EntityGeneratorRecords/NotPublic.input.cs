﻿using DKX.Pg.Entities;

namespace DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords
{
	public sealed class EntityParent
	{
		[Entity]
		private partial record NotPublic
		{
		}
	}
}
