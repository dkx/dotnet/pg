﻿using DKX.Pg.Entities;

namespace DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords
{
	[Entity]
	public partial record CustomNames
	{
		[Name("full_name")]
		public string Name { get; }
	}
}
