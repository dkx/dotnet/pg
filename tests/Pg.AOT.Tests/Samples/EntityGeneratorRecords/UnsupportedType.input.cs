﻿using DKX.Pg.Entities;

namespace DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords
{
	[Entity]
	public partial record UnsupportedType
	{
		public uint Prop { get; }
	}
}
