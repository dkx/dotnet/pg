﻿namespace DKX.Pg.AOT.Tests.Samples.ValidEntityMappingGeneratorSplit
{
	public sealed class GetResultGeneric
	{
		private readonly IConnection _db;

		public GetResultGeneric(IConnection db)
		{
			_db = db;
		}

		public void Call<T>()
			where T : class
		{
			var query = _db.Query($"SELECT * FROM users");
			var result = query.GetResult<T>();
		}
	}
}
