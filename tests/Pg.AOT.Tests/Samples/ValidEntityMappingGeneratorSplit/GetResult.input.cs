﻿using DKX.Pg.Entities;

namespace DKX.Pg.AOT.Tests.Samples.ValidEntityMappingGeneratorSplit
{
	public sealed class GetResult
	{
		private readonly IConnection _db;

		public GetResult(IConnection db)
		{
			_db = db;
		}

		public void Call()
		{
			var query = _db.Query($"SELECT * FROM users");
			var result = query.GetResult<GetResultEntity>();
		}
	}

	[Entity]
	public partial class GetResultEntity
	{
	}
}
