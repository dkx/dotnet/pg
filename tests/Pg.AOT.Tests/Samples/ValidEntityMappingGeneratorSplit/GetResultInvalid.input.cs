﻿namespace DKX.Pg.AOT.Tests.Samples.ValidEntityMappingGeneratorSplit
{
	public sealed class GetResultInvalid
	{
		private readonly IConnection _db;

		public GetResultInvalid(IConnection db)
		{
			_db = db;
		}

		public void Call()
		{
			var query = _db.Query($"SELECT * FROM users");
			var result = query.GetResult<GetResultInvalidEntity>();
		}
	}

	public sealed class GetResultInvalidEntity
	{
	}
}
