﻿using System.Threading.Tasks;
using DKX.Pg.AOT.Generators;
using Microsoft.CodeAnalysis;
using Xunit;

namespace DKX.Pg.AOT.Tests.Generators
{
	public sealed class ValidEntityMappingGeneratorChainedTest
	{
		[Fact]
		public async Task GetResult()
		{
			await Helpers.VerifySourceGenerator<ValidEntityMappingGenerator>("ValidEntityMappingGeneratorChained", "GetResult");
		}
		
		[Fact]
		public async Task GetResult_ErrorNotEntity()
		{
			await Helpers.VerifySourceGenerator<ValidEntityMappingGenerator>("ValidEntityMappingGeneratorChained", "GetResultInvalid", new []
			{
				new DiagnosticInfo(DiagnosticSeverity.Error, "DKXPG0004", "Mapping data to non-entity type", "Could not map data into 'DKX.Pg.AOT.Tests.Samples.ValidEntityMappingGeneratorChained.GetResultInvalidEntity', target type must be a partial class/record with [Entity] attribute"),
			});
		}
		
		[Fact]
		public async Task GetResult_WarningGenericType()
		{
			await Helpers.VerifySourceGenerator<ValidEntityMappingGenerator>("ValidEntityMappingGeneratorChained", "GetResultGeneric", new []
			{
				new DiagnosticInfo(DiagnosticSeverity.Warning, "DKXPG0005", "Mapping data to generic type", "Could not analyze mapping to a generic type"),
			});
		}
		
		[Fact]
		public async Task GetResult_MultiWithErrors()
		{
			await Helpers.VerifySourceGenerator<ValidEntityMappingGenerator>("ValidEntityMappingGeneratorChained", "GetResultMulti", new []
			{
				new DiagnosticInfo(DiagnosticSeverity.Error, "DKXPG0004", "Mapping data to non-entity type", "Could not map data into 'DKX.Pg.AOT.Tests.Samples.ValidEntityMappingGeneratorChained.GetResultMultiEntityB', target type must be a partial class/record with [Entity] attribute"),
				new DiagnosticInfo(DiagnosticSeverity.Error, "DKXPG0004", "Mapping data to non-entity type", "Could not map data into 'DKX.Pg.AOT.Tests.Samples.ValidEntityMappingGeneratorChained.GetResultMultiEntityD', target type must be a partial class/record with [Entity] attribute"),
			});
		}
	}
}
