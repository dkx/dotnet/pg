﻿using System.Threading.Tasks;
using DKX.Pg.AOT.Generators;
using Microsoft.CodeAnalysis;
using Xunit;

namespace DKX.Pg.AOT.Tests.Generators
{
	public sealed class ValidEntityMappingGeneratorSplitTest
	{
		[Fact]
		public async Task GetResult()
		{
			await Helpers.VerifySourceGenerator<ValidEntityMappingGenerator>("ValidEntityMappingGeneratorSplit", "GetResult");
		}
		
		[Fact]
		public async Task GetResult_ErrorNotEntity()
		{
			await Helpers.VerifySourceGenerator<ValidEntityMappingGenerator>("ValidEntityMappingGeneratorSplit", "GetResultInvalid", new []
			{
				new DiagnosticInfo(DiagnosticSeverity.Error, "DKXPG0004", "Mapping data to non-entity type", "Could not map data into 'DKX.Pg.AOT.Tests.Samples.ValidEntityMappingGeneratorSplit.GetResultInvalidEntity', target type must be a partial class/record with [Entity] attribute"),
			});
		}
		
		[Fact]
		public async Task GetResult_WarningGenericType()
		{
			await Helpers.VerifySourceGenerator<ValidEntityMappingGenerator>("ValidEntityMappingGeneratorSplit", "GetResultGeneric", new []
			{
				new DiagnosticInfo(DiagnosticSeverity.Warning, "DKXPG0005", "Mapping data to generic type", "Could not analyze mapping to a generic type"),
			});
		}
	}
}
