﻿using System.Threading.Tasks;
using DKX.Pg.AOT.Generators;
using Microsoft.CodeAnalysis;
using Xunit;

namespace DKX.Pg.AOT.Tests.Generators
{
	public sealed class EntityImplementationGeneratorRecordsTest
	{
		[Theory]
		[InlineData("DoNothing")]
		[InlineData("PropertiesNotNullable")]
		[InlineData("PropertiesNullable")]
		[InlineData("CustomNames")]
		[InlineData("RecordEntity")]
		public async Task Generate(string sampleName)
		{
			await Helpers.VerifySourceGenerator<EntityGenerator>("EntityGeneratorRecords", sampleName);
		}

		[Fact]
		public async Task Generate_ErrorEntityNotPartial()
		{
			await Helpers.VerifySourceGenerator<EntityGenerator>("EntityGeneratorRecords", "NotPartial", new[]
			{
				new DiagnosticInfo(DiagnosticSeverity.Error, "DKXPG0002", "Entity must be partial", "Entity class 'DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords.NotPartial' must be marked as partial"),
			});
		}

		[Fact]
		public async Task Generate_ErrorEntityNotPublic()
		{
			await Helpers.VerifySourceGenerator<EntityGenerator>("EntityGeneratorRecords", "NotPublic", new[]
			{
				new DiagnosticInfo(DiagnosticSeverity.Error, "DKXPG0003", "Entity must be public", "Entity class 'DKX.Pg.AOT.Tests.Samples.EntityGeneratorRecords.NotPublic' must be marked as public"),
			});
		}
	}
}
