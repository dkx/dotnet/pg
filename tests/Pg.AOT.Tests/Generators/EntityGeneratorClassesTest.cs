﻿using System.Threading.Tasks;
using DKX.Pg.AOT.Generators;
using Microsoft.CodeAnalysis;
using Xunit;

namespace DKX.Pg.AOT.Tests.Generators
{
	public sealed class EntityImplementationGeneratorClassesTest
	{
		[Theory]
		[InlineData("DoNothing")]
		[InlineData("PropertiesNotNullable")]
		[InlineData("PropertiesNullable")]
		[InlineData("CustomNames")]
		[InlineData("RecordEntity")]
		public async Task Generate(string sampleName)
		{
			await Helpers.VerifySourceGenerator<EntityGenerator>("EntityGeneratorClasses", sampleName);
		}

		[Fact]
		public async Task Generate_ErrorEntityNotPartial()
		{
			await Helpers.VerifySourceGenerator<EntityGenerator>("EntityGeneratorClasses", "NotPartial", new[]
			{
				new DiagnosticInfo(DiagnosticSeverity.Error, "DKXPG0002", "Entity must be partial", "Entity class 'DKX.Pg.AOT.Tests.Samples.EntityGeneratorClasses.NotPartial' must be marked as partial"),
			});
		}

		[Fact]
		public async Task Generate_ErrorEntityNotPublic()
		{
			await Helpers.VerifySourceGenerator<EntityGenerator>("EntityGeneratorClasses", "NotPublic", new[]
			{
				new DiagnosticInfo(DiagnosticSeverity.Error, "DKXPG0003", "Entity must be public", "Entity class 'DKX.Pg.AOT.Tests.Samples.EntityGeneratorClasses.NotPublic' must be marked as public"),
			});
		}
	}
}
