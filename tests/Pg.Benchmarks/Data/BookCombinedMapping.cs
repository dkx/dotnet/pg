﻿namespace DKX.Pg.Benchmarks.Data
{
	public sealed class BookCombinedMapping
	{
		public BookCombinedMapping(int id, string name)
		{
			Id = id;
			Name = name;
		}

		public int Id { get; }

		public string Name { get; }
		
		public string? AccessKey { get; init; }
	}
}
