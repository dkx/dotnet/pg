﻿using System.Data;

namespace DKX.Pg.Benchmarks.Data
{
	public sealed class BookWithDataRecordMapping
	{
		public BookWithDataRecordMapping(IDataRecord record)
		{
			Id = record.GetInt32(0);
			Name = record.GetString(1);
			AccessKey = record.IsDBNull(2) ? null : record.GetString(2);
		}

		public int Id { get; }

		public string Name { get; }
		
		public string? AccessKey { get; }
	}
}
