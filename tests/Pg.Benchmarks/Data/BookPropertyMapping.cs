﻿namespace DKX.Pg.Benchmarks.Data
{
	public sealed class BookPropertyMapping
	{
		public int Id { get; init; }

		public string Name { get; init; }
		
		public string? AccessKey { get; init; }
	}
}
