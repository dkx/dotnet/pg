﻿namespace DKX.Pg.Benchmarks.Data
{
	public sealed class BookConstructorMapping
	{
		public BookConstructorMapping(int id, string name, string? accessKey)
		{
			Id = id;
			Name = name;
			AccessKey = accessKey;
		}

		public int Id { get; }

		public string Name { get; }
		
		public string? AccessKey { get; }
	}
}
