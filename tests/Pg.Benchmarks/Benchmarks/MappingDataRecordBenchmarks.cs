﻿using System.Data;
using BenchmarkDotNet.Attributes;
using DKX.Pg.Benchmarks.Data;
using DKX.Pg.Metadata;

namespace DKX.Pg.Benchmarks.Benchmarks
{
	[SimpleJob]
	[MemoryDiagnoser]
	public class MappingDataRecordBenchmarks
	{
		private readonly IEntityMetadata<BookWithDataRecordMapping> _entityType = Utils.LoadEntityMapping<BookWithDataRecordMapping>();

		private readonly IDataRecord _record = Utils.CreateBookRecord();

		private readonly int[] _mapping = { 0, 1, 2 };
		
		[Benchmark(Baseline = true)]
		public BookWithDataRecordMapping MappingDataRecordManually()
		{
			return new BookWithDataRecordMapping(_record);
		}
		
		[Benchmark]
		public BookWithDataRecordMapping MappingConstructorWithDefaultEntityType()
		{
			return _entityType.CreateEntity(_record, _mapping);
		}
	}
}
