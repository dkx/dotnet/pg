﻿using BenchmarkDotNet.Attributes;
using DKX.Pg.Benchmarks.Data;
using DKX.Pg.Metadata;
using DKX.Pg.Parsing;
using DKX.Pg.Snippets;

namespace DKX.Pg.Benchmarks.Benchmarks
{
	[SimpleJob]
	[MemoryDiagnoser]
	public class ColumnsSnippetBenchmarks
	{
		private readonly ColumnsSnippet<BookConstructorMapping> _snippet = new ColumnsSnippet<BookConstructorMapping>();
		
		private readonly ColumnsSnippet<BookConstructorMapping> _snippetWithAlias = new ColumnsSnippet<BookConstructorMapping>();

		private readonly IQueryParsingContext _ctx;

		public ColumnsSnippetBenchmarks()
		{
			_ctx = new QueryParsingContext(new SqlParser(), new EntitiesMetadataRegistry(new AssemblyEntitiesMetadataProvider(GetType().Assembly)), false);
		}

		[Benchmark]
		public string ProcessColumns()
		{
			return _snippet.Process(_ctx, null);
		}

		[Benchmark]
		public string ProcessColumnsWithAlias()
		{
			return _snippetWithAlias.Process(_ctx, null);
		}
	}
}
