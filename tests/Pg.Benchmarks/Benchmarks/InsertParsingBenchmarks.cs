﻿using BenchmarkDotNet.Attributes;
using DKX.Pg.Benchmarks.Data;
using DKX.Pg.Metadata;
using DKX.Pg.Parsing;
using DKX.Pg.Snippets;

namespace DKX.Pg.Benchmarks.Benchmarks
{
	[SimpleJob]
	[MemoryDiagnoser]
	public class InsertParsingBenchmarks
	{
		private readonly ISqlParser _parser;

		private readonly IEntitiesMetadataRegistry _mappingsRegistry;

		private readonly BookPropertyMapping _entity;

		public InsertParsingBenchmarks()
		{
			_parser = new SqlParser();
			_mappingsRegistry = new EntitiesMetadataRegistry(new AssemblyEntitiesMetadataProvider(GetType().Assembly));
			_entity = new BookPropertyMapping
			{
				Id = 42,
				Name = "Harry Potter",
				AccessKey = "abcd",
			};
		}

		[Benchmark]
		public string InsertParsing()
		{
			return _parser.Parse(
				$"INSERT INTO \"{new RawSnippet("books")}\" {new ValuesSnippet<BookPropertyMapping>(_entity)}",
				new QueryParsingContext(_parser, _mappingsRegistry, false)
			);
		}
	}
}
