﻿using System.Data;
using BenchmarkDotNet.Attributes;
using DKX.Pg.Benchmarks.Data;
using DKX.Pg.Metadata;

namespace DKX.Pg.Benchmarks.Benchmarks
{
	[SimpleJob]
	[MemoryDiagnoser]
	public class MappingConstructorBenchmarks
	{
		private readonly IEntityMetadata<BookConstructorMapping> _entityType = Utils.LoadEntityMapping<BookConstructorMapping>();

		private readonly IDataRecord _record = Utils.CreateBookRecord();

		private readonly int[] _mapping = { 0, 1, 2 };
		
		[Benchmark(Baseline = true)]
		public BookConstructorMapping MappingConstructorManually()
		{
			return new BookConstructorMapping(
				_record.GetInt32(0),
				_record.GetString(1),
				_record.IsDBNull(2) ? null : _record.GetString(2)
			);
		}
		
		[Benchmark]
		public BookConstructorMapping MappingConstructorWithDefaultEntityType()
		{
			return _entityType.CreateEntity(_record, _mapping);
		}
	}
}
