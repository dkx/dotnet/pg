﻿using System.Data;
using BenchmarkDotNet.Attributes;
using DKX.Pg.Benchmarks.Data;
using DKX.Pg.Metadata;

namespace DKX.Pg.Benchmarks.Benchmarks
{
	[SimpleJob]
	[MemoryDiagnoser]
	public class MappingCombinedBenchmarks
	{
		private readonly IEntityMetadata<BookCombinedMapping> _entityType = Utils.LoadEntityMapping<BookCombinedMapping>();

		private readonly IDataRecord _record = Utils.CreateBookRecord();

		private readonly int[] _mapping = { 0, 1, 2 };
		
		[Benchmark(Baseline = true)]
		public BookCombinedMapping MappingCombinedManually()
		{
			return new BookCombinedMapping(_record.GetInt32(0), _record.GetString(1))
			{
				AccessKey = _record.IsDBNull(2) ? null : _record.GetString(2),
			};
		}
		
		[Benchmark]
		public BookCombinedMapping MappingCombinedWithDefaultEntityType()
		{
			return _entityType.CreateEntity(_record, _mapping);
		}
	}
}
