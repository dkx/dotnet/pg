﻿using System.Data;
using BenchmarkDotNet.Attributes;
using DKX.Pg.Benchmarks.Data;
using DKX.Pg.Metadata;

namespace DKX.Pg.Benchmarks.Benchmarks
{
	[SimpleJob]
	[MemoryDiagnoser]
	public class MappingPropertyBenchmarks
	{
		private readonly IEntityMetadata<BookPropertyMapping> _entityType = Utils.LoadEntityMapping<BookPropertyMapping>();

		private readonly IDataRecord _record = Utils.CreateBookRecord();

		private readonly int[] _mapping = { 0, 1, 2 };
		
		[Benchmark(Baseline = true)]
		public BookPropertyMapping MappingPropertyManually()
		{
			return new BookPropertyMapping
			{
				Id = _record.GetInt32(0),
				Name = _record.GetString(1),
				AccessKey = _record.IsDBNull(2) ? null : _record.GetString(2),
			};
		}
		
		[Benchmark]
		public BookPropertyMapping MappingPropertyWithDefaultEntityType()
		{
			return _entityType.CreateEntity(_record, _mapping);
		}
	}
}
