﻿using System.Data;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Order;
using BenchmarkDotNet.Running;
using DKX.Pg.Benchmarks.Data;

namespace DKX.Pg.Benchmarks
{
	class Program
	{
		static void Main(string[] args)
		{
			var config = ManualConfig
				.Create(DefaultConfig.Instance)
				.WithOptions(ConfigOptions.DisableLogFile)
				.WithOptions(ConfigOptions.JoinSummary)
				.WithOrderer(new DefaultOrderer(SummaryOrderPolicy.Declared));

			BenchmarkSwitcher.FromAssembly(typeof(Program).Assembly).RunAll(config);
		}

		private static BookWithDataRecordMapping Create(IDataRecord record)
		{
			return new BookWithDataRecordMapping(record);
		}
	}
}
