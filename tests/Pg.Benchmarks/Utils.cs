﻿using System;
using System.Data;
using DKX.Pg.Metadata;

namespace DKX.Pg.Benchmarks
{
	internal static class Utils
	{
		public static IEntityMetadata<T> LoadEntityMapping<T>()
			where T : class
		{
			var typesLoader = new EntitiesMetadataRegistry(new AssemblyEntitiesMetadataProvider(typeof(Utils).Assembly));
			return typesLoader.GetMetadata<T>();
		}

		public static IDataRecord CreateBookRecord()
		{
			var table = new DataTable();
			
			table.Columns.Add(new DataColumn("id", typeof(int)));
			table.Columns.Add(new DataColumn("name", typeof(string)));
			table.Columns.Add(new DataColumn("access_key", typeof(string)));

			var row = table.NewRow();
			row["id"] = 42;
			row["name"] = "Harry Potter";
			row["access_key"] = null;
			table.Rows.Add(row);

			var reader =  new DataTableReader(table);

			while (reader.Read())
			{
				return reader;
			}

			throw new Exception("No record");
		}
	}
}
