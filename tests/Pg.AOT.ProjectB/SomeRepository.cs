﻿using System.Threading.Tasks;
using DKX.Pg.AOT.ProjectA;

namespace DKX.Pg.AOT.ProjectB
{
	public class SomeRepository
	{
		private readonly IConnection _db;

		public SomeRepository(IConnection db)
		{
			_db = db;
		}

		public async Task<SomeEntity> Load()
		{
			return await _db
				.Query($"SELECT * FROM some_table")
				.GetSingleResult<SomeEntity>();
		}
	}
}
