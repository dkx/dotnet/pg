# Snippets

Snippets are special building blocks for easier construction of your queries. They are the parts of code in curly braces 
within your queries.

## Build in snippets

### Parameter snippet

`ParameterSnippet` takes the input and transforms it into SQL parameter.

```c#
db.Query($"SELECT * FROM users WHERE email = {email}");
```

which is just a shortcut for:

```c#
db.Query($"SELECT * FROM users WHERE email = {db.Param(email)}");
```

or with strongly-typed parameters:

```c#
db.Query($"SELECT * FROM users WHERE email = {db.Param<string>(email)}");
```

**output SQL:**

```sql
SELECT * FROM users WHERE email = $1
```

### Columns snippet

`ColumnsSnippet` writes all columns.

```c#
db.Query($"SELECT {db.Columns<User>()} FROM users");
```

**output SQL:**

```sql
SELECT "id", "email" FROM "users"
```

Or with table alias:

```c#
db.Query($"SELECT {db.Columns<User>("u")} FROM users u");
```

**output SQL:**

```sql
SELECT u."id", u."email" FROM "users" u
```

### Composite snippet

`CompositeSnippet` is useful for generating dynamic `WHERE` clauses.

```c#
var email1 = "john@doe.com";
var email2 = "doe@john.com";

// there is also And method
var where = db.Or(
    $"email = {email1}",
    $"email = {email2}"
);

db.Query($"SELECT * FROM users WHERE {where}");
```

**output SQL:**

```sql
SELECT * FROM users WHERE (email = $1) OR (email = $2)
```

### If snippet

`IfSnippets` writes SQL based on result of condition. Useful for conditional SQL parts. 

```c#
var where = db.Or(
    $"email = {email1}",
    $"email = {email2}"
);

db.Query($"SELECT * FROM users {db.If(where.Count > 0, $"WHERE {where}")}");
```

**output SQL:**

```sql
INSERT INTO users (name, email) VALUES ($1, $2)
```

### List snippet

`ListSnippet` writes provided list of strings separated by `, `.

```c#
var orderBy = db.List(
    $"name ASC",
    $"email DESC",
);

db.Query($"SELECT * FROM users ORDER BY {orderBy}");
```

**output SQL:**

```sql
SELECT * FROM users ORDER BY name ASC, email DESC
```

### Raw snippet

`RawSnippets` writes everything as is.

```c#
var literal = db.Raw("id, email");
db.Query($"SELECT {literal} FROM users");
```

**output SQL:**

```sql
SELECT id, email FROM users
```

### SetDictionary snippet

`SetDictionarySnippet` writes all values from provided dictionary as a valid `SET` clause values.

```c#
var values = new Dictionary<string, object?>
{
    ["name"] = "John Doe",
    ["email"] = "john@doe.com",
};

db.Query($"UPDATE users SET {db.SetDictionary(values)} WHERE id = {id}");
```

**output SQL:**

```sql
UPDATE users SET "name" = $1, "email" = $2 WHERE id = $3
```

### Set snippet

`SetSnippet` writes all values from entity as a valid `SET` clause values.

```c#
var user = new User
{
    Id = 3,
    Name = "John Doe",
    Email = "john@doe.com",
};

db.Query($"UPDATE users SET {db.Set(user)} WHERE id = {user.Id}");
```

**output SQL:**

```sql
UPDATE users SET "name" = $1, "email" = $2 WHERE id = $3
```

### ValuesDictionary snippet

`ValuesDictionarySnippets` writes all values from provided dictionary as a valid `INSERT VALUES` clause values.

```c#
var values = new Dictionary<string, object?>
{
    ["name"] = "John Doe",
    ["email"] = "john@doe.com",
};

db.Query($"INSERT INTO users {db.Dictionary(values)}");
```

**output SQL:**

```sql
INSERT INTO users ("name", "email") VALUES ($1, $2)
```

### Values snippet

`ValuesSnippets` writes all values from entity as a valid `INSERT VALUES` clause values.

```c#
var user = new User
{
    Id = 3,
    Name = "John Doe",
    Email = "john@doe.com",
};

db.Query($"INSERT INTO users {db.Values(user)}");
```

**output SQL:**

```sql
INSERT INTO users ("id", "name", "email") VALUES ($1, $2, $3)
```

## Writing custom snippets

Every snippet must implement a simple interface `DKX.Pg.Parsing.ISnippet`. It contains `Process` method that returns 
the result string.

Let's reimplement the [Raw snippet](#raw-snippet):

```c#
using DKX.Pg.Parsing;

class MyRawSnippet : ISnippet
{
    private readonly string _value;
    
    public MyRawSnippet(string value)
    {
        _value = value;
    }

    public string Process(IQueryParsingContext ctx, string? format)
    {
        return _value;
    }
}
```

Now `MyRawSnippet` can be used like this:

```c#
db.Query($"{new MyRawSnippet("SELECT * FROM users")}");
```

That does not look nice, so let's write an extension method for convenience too:

```c#
using DKX.Pg;

static class MyRawSnippetConnectionExtensions
{
    public static MyRawSnippet MyRaw(this IConnection db, string value)
    {
        return new MyRawSnippet(value);
    }
}
```

The usage is now better:

```c#
db.Query($"{db.MyRaw("SELECT * FROM users")}");
```

---

When writing custom snippets you have access to `DKX.Pg.Parsing.IQueryParsingContext`. The `IQueryParsingContext`
contains information about currently processed query and also some helper methods:

* `string AddParameter(object? value)`: creates new parameter and returns the generated name
* `string AddParameter<T>(T value)`: creates new strongly typed parameter and returns the generated name (see [npgsql.org docs](https://www.npgsql.org/doc/basic-usage.html#strongly-typed-parameters))
* `string ParseSqlPart(FormattableString part)`: allows you to parse nested/inner sql parts
* `string Quote(string str)`: returns quoted string
* `IEntityMetadata<T> LoadEntityMetadata<T>()`: returns metadata for given entity type
