# Transactions

```c#
await db.Transactional(async () => {
    // todo: write SQL here
});
```

You can nest the `Transactional` calls as much as you may need. Nested transactions are supported via savepoints.

## With `using`

```c#
await using (var t1 = await Db.BeginTransaction())
{
    // todo: write SQL here
    await t1.Commit();
}
```

Nesting is supported too.

If you forget to call the `Commit()` method, `Rollback()` will be called automatically on disposal.
