﻿# Mapping

The `DKX.Pg.AOT` package automatically creates mapping classes when building your project using the 
[source generators](https://docs.microsoft.com/en-us/dotnet/csharp/roslyn-sdk/source-generators-overview).

There are some requirements to make this work:

* Only .net 5 projects are supported
* All entities must have the `[Entity]` class attribute
* All entities must be `public`
* All entities must be `partial`

**Example entity:**

```c#
use DKX.Pg.Entities;

[Entity]
public partial class User
{
    [Id, Generated]
    public int Id { get; }

    public string Email { get; }
}
```

## Ignored properties

Add `[NotMapped]` attribute to properties that should be ignored by this package.

```c#
use DKX.Pg.Entities;

[Entity]
public partial class User
{
    [Id]
    public Guid Id { get; }
    
    public string Name { get; }
    
    [NotMapped]
    public string Greetings => $"Hello {Name}";
}
```

## Custom column name

By default column names are underscored property names. You can override this with `[Name]` attribute.

```c#
use DKX.Pg.Entities;

[Entity]
public partial class User
{
    [Id]
    public Guid Id { get; }
    
    [Name("full_name")]
    public string Name { get; }
}
```

## How the mapping works

This package automatically adds a new custom constructor to all your entities. This new constructor is then used when 
fetching and mapping data from database

**Example of the autogenerated class:**

```c#
public partial class User
{
    public User(global::System.Data.IDataRecord record, int[] mapping)
    {
        Id = record.GetInt32(mapping[0]);
        Email = record.GetString(mapping[1]);
    }
}
```
