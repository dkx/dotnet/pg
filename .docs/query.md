# Query

```c#
var query = db.Query($"SELECT * FROM users");
```

You may have noticed in the example above that we are using `FormattedString` instead of ordinary value type `string`.

The reason is that `Query` method expects `FormattedString` and not `string`. You can read more about `FormattedString` 
for example here - [Advanced String Templates in C# by Eric Damtoft](https://dev.to/dealeron/advanced-string-templates-in-c-2eh2).

## Passing parameters to SQL

Setting parameters couldn't be any easier when writing plain old SQL and using `FormattedString` from C#.

You can read more about this feature in [snippets](snippets.md).

**Basic parameters:**

```c#
var email1 = "john@doe.com";
var email2 = "doe@john.com";

db.Query($"SELECT * FROM users WHERE email = {email1} OR email = {email2}");
```

**Strongly-typed parameters:**

[Npgsql documentation](https://www.npgsql.org/doc/basic-usage.html#strongly-typed-parameters)

```c#
var email1 = db.Param<string>("john@doe.com");
var email2 = db.Param<string>("doe@john.com");

db.Query($"SELECT * FROM users WHERE email = {email1} OR email = {email2}");
```

## Results

### GetResult

Returns `IAsyncEnumerable<T> where T : class`

```c#
await foreach (var user in db.Query($"SELECT * FROM users").GetResult<User>())
{
    // todo
}
```

### GetScalarResult

Returns `IAsyncEnumerable<T>`

```c#
await foreach (var email in db.Query($"SELECT email FROM users").GetScalarResult<string>())
{
    // todo
}
```

### GetSingleOrNullResult

Returns `Task<T?> where T : class`

```c#
User? user = await db.Query($"SELECT * FROM users WHERE id = {id}").GetSingleOrNullResult<User>();
```

### GetSingleResult

Returns `Task<T> where T : class`

```c#
User user = await db.Query($"SELECT * FROM users WHERE id = {id}").GetSingleResult<User>();
```

### GetSingleScalarResult

Returns `Task<T>`

```c#
string email = await db.Query($"SELECT email FROM users WHERE id = {id}").GetSingleScalarResult<string>();
```

### Execute

Returns `Task<int>`

Returns `Task<T>`

```c#
int affectedRows = await db.Query($"UPDATE users SET removed_at = now()").Execute();
```

## Shortcuts

### Insert

```c#
var table = "users";
await db.Insert(table, new User(Guid.NewGuid(), "john@doe.com"));
```

**Generated SQL:**

```sql
INSERT INTO "users" (id, email) VALUES ($1, $2)
```

### Update

```c#
var table = "users";
await db.Update(table, user);
```

**Generated SQL:**

```sql
UPDATE "users" SET email = $1 WHERE (id = $2)
```

## Raw query

```c#
await db
    .QueryRaw("SELECT COUNT(*) FROM \"users\"")
    .GetSingleScalarResult<long>();
```
