# Logging

```c#
using DKX.Pg.Events;

class MyEventListener : EventListener
{
    public override void OnQueryStart(QueryPayload query)
    {
        // todo: log query start
    }
    
    public override void OnQueryStop(QueryPayload query, Exception? exception)
    {
        // todo: log query end
    }
}

db.AddEventListener(new MyEventListener());
```
