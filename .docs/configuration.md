# Configuration

```c#
using DKX.Pg;
using DKX.Pg.Mapping;

var options = new ConnectionOptions
{
    AutoOpen = true,
    UseLegacyNamedParameters = false,
    DisposeNativeConnection = true,
    PrepareCommands = true,
    AutoConnector = new DefaultAutoConnector(npgsqlConnection),
    TransactionsManager = new DefaultTransactionsManager(),
};

await using var db = new Connection(npgsqlConnection, metadataProvider, options);
```

All options are optional and the example above shows the default values.

* `AutoOpen`: indicates whether automatically open the connection right before communicating with the database
* `UseLegacyNamedParameters`: generate named parameters (eg. `@p2` instead of `$3`). Read more [here](https://www.npgsql.org/doc/release-notes/6.0.html#raw-sql-mode-and-new-batching-api) or [here](https://www.roji.org/parameters-batching-and-sql-rewriting)
* `DisposeNativeConnection`: disposes provided native npgsql connection when disposing `DKX.Pg.Connection`
* `PrepareCommands`: use prepared statements
